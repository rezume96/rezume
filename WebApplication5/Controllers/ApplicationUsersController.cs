﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class ApplicationUsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ApplicationUsers
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        // GET: ApplicationUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,pname,shenasname,D_tavalod,mojarad,jens,khedmat,Mobile,Address,shahr,eshteghal,pictur,tozih,alaghe,mahalkar,hoghogh,profil,tedadrezkary,tedadreztahsil,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(applicationUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationUser);
        }

        // GET: Users/Edit/5
        public ActionResult Edit()
        {
            checkController c = new checkController();
            string s = c.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            db.Users.Where(f => f.Email == s).Load();

            if (db.Users.Local == null)
            {
                return HttpNotFound();
            }
            return View(db.Users.Local[0]);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,pname,shenasname,D_tavalod,mojarad,jens,khedmat,nomoaf,payankhedmat,Mobile,Address,shahr,eshteghal,pictur,tozih,alaghe,mahalkar,hoghogh,profil,tedadrezkary,tedadreztahsil,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser user1, HttpPostedFileBase UploadFile)
        {
            checkController r = new checkController();
            string s = r.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                ApplicationDbContext fff = new ApplicationDbContext();
                fff.Users.Where(f => f.profil == user1.profil).Load();
                if (fff.Users.Local.Count != 0)
                {
                    if (fff.Users.Local[0].Id != user1.Id)
                    {
                        ModelState.AddModelError("", "ادرس پروفایل قبلا وجود دارد لطفا ادرس دیگری انتخاب نمایید.");
                        return View(user1);
                    }
                }
                var j = user1.profil.Split('.');
                if (j.Length > 1)
                {
                    ModelState.AddModelError("", "ادرس پروفایل غیر مجاز است.از نقطه استفاده نکنید.");
                    return View(user1);
                }

                //if (user1.shenasname == "" || user1.shenasname == null || user1.shenasname == string.Empty)
                //{
                    if (UploadFile != null)
                    {



                        //میتوان از طریق این متغیر حجم فایل قبل از آپلودرا مورد بررسی قرارداد
                        System.Drawing.Image oImage =
                           System.Drawing.Image.FromStream(UploadFile.InputStream);

                        //میتوان از طریق این متغیر پسوند فایل تصویری را مورد بررسی قرارداد
                        string strFileExtension =
                        System.IO.Path.GetExtension(UploadFile.FileName).ToUpper();

                        //میتوان از طریق این متغیر ماهیت فایل تصویری را مورد بررسی قرارداد
                        string strContentType =
                        UploadFile.ContentType.ToUpper();




                        if (/*(oImage.Width > 1024) || (oImage.Height > 768)

                    ||*/ (UploadFile.FileName.Trim() == string.Empty))
                        {
                            return View("Error");
                        }

                        else
                        {
                            user1.pictur = user1.Id + UploadFile.FileName;

                            string strPath = Server.MapPath("~") + "MyFiles\\";

                            if (System.IO.Directory.Exists(strPath) == false)
                            {
                                System.IO.Directory.CreateDirectory(strPath);
                            }

                            string strPathName =
                                string.Format("{0}\\{1}", strPath, user1.pictur);

                            UploadFile.SaveAs(strPathName);
                        }
                    }
                    ApplicationDbContext db1 = new ApplicationDbContext();
                    var temp = db1.Users.Where(f => f.Id == user1.Id).FirstOrDefault();
                    temp.Name = user1.Name;
                    temp.pname = user1.pname;
                    temp.shenasname = user1.shenasname;
                    temp.D_tavalod = user1.D_tavalod;
                    temp.mojarad = user1.mojarad;
                    temp.jens = user1.jens;
                    temp.khedmat = user1.khedmat;
                    temp.nomoaf = user1.nomoaf;
                    temp.payankhedmat = user1.payankhedmat;
                    temp.Mobile = user1.Mobile;
                    temp.Address = user1.Address;
                    temp.shahr = user1.shahr;
                    temp.eshteghal = user1.eshteghal;
                    temp.pictur = user1.pictur;
                    temp.tozih = user1.tozih;
                    temp.tedadrezkary = user1.tedadrezkary;
                    temp.tedadreztahsil = user1.tedadreztahsil;
                    temp.Email = user1.Email;
                    temp.PhoneNumber = user1.PhoneNumber;
                    temp.profil = user1.profil;
                    db1.SaveChanges();
                    return RedirectToAction("show", new { controller = "pdfs1", action = "show" });
                //}
                //چک کردن کد ملی
                //char[] chArray = user1.shenasname.ToCharArray();
                //int[] numArray = new int[chArray.Length];
                //for (int i = 0; i < chArray.Length; i++)
                //{
                //    numArray[i] = (int)char.GetNumericValue(chArray[i]);
                //}
                //if (chArray.Length != 10)
                //{
                //    ModelState.AddModelError("", "شماره شناسنامه خود را به درستی وارد نمایید");
                //    return View(user1);
                //}
                //int num2 = numArray[9];
                //switch (user1.shenasname)
                //{
                //    case "0000000000":
                //    case "1111111111":
                //    case "22222222222":
                //    case "33333333333":
                //    case "4444444444":
                //    case "5555555555":
                //    case "6666666666":
                //    case "7777777777":
                //    case "8888888888":
                //    case "9999999999":
                //        return View(user1);
                //}
                //int num3 = ((((((((numArray[0] * 10) + (numArray[1] * 9)) + (numArray[2] * 8)) + (numArray[3] * 7)) + (numArray[4] * 6)) + (numArray[5] * 5)) + (numArray[6] * 4)) + (numArray[7] * 3)) + (numArray[8] * 2);
                //int num4 = num3 - ((num3 / 11) * 11);
                /////اگر کد ملی درست بود
                //if ((((num4 == 0) && (num2 == num4)) || ((num4 == 1) && (num2 == 1))) || ((num4 > 1) && (num2 == Math.Abs((int)(num4 - 11)))))
                //{
                //    db.Users.Where(c => c.shenasname == user1.shenasname).Load();
                //    string id = "";
                //    if (db.Users.Local.Count != 0)
                //    {
                //        id = db.Users.Local[0].Id;
                //    }
                //    //چک کردن وجود کد ملی
                //    if (db.Users.Local.Count == 0 || id == user1.Id)
                //    {
                //        if (UploadFile != null)
                //        {
                //            string strFileExtension =
                //           System.IO.Path.GetExtension(UploadFile.FileName).ToUpper();

                //            //میتوان از طریق این متغیر ماهیت فایل را مورد بررسی قرارداد
                //            string strContentType =
                //            UploadFile.ContentType.ToUpper();


                //            if ((UploadFile == null
                //            || (UploadFile.ContentLength == 0)
                //            || (UploadFile.ContentLength > 12000 * 1024)))

                //            {
                //                return View("Error");
                //            }

                //            else
                //            {
                //                user1.pictur = user1.Id + UploadFile.FileName;

                //                string strPath = Server.MapPath("~") + "MyFiles\\";

                //                if (System.IO.Directory.Exists(strPath) == false)
                //                {
                //                    System.IO.Directory.CreateDirectory(strPath);
                //                }

                //                string strPathName =
                //                    string.Format("{0}\\{1}", strPath, user1.pictur);

                //                UploadFile.SaveAs(strPathName);
                //            }
                //        }
                //        ApplicationDbContext db1 = new ApplicationDbContext();
                //        var temp = db1.Users.Where(f => f.Id == user1.Id).FirstOrDefault();
                //        temp.Name = user1.Name;
                //        temp.pname = user1.pname;
                //        temp.shenasname = user1.shenasname;
                //        temp.D_tavalod = user1.D_tavalod;
                //        temp.mojarad = user1.mojarad;
                //        temp.jens = user1.jens;
                //        temp.khedmat = user1.khedmat;
                //        temp.nomoaf = user1.nomoaf;
                //        temp.payankhedmat = user1.payankhedmat;
                //        temp.Mobile = user1.Mobile;
                //        temp.Address = user1.Address;
                //        temp.shahr = user1.shahr;
                //        temp.eshteghal = user1.eshteghal;
                //        temp.pictur = user1.pictur;
                //        temp.tozih = user1.tozih;
                //        temp.tedadrezkary = user1.tedadrezkary;
                //        temp.tedadreztahsil = user1.tedadreztahsil;
                //        temp.Email = user1.Email;
                //        temp.PhoneNumber = user1.PhoneNumber;
                //        temp.profil = user1.profil;
                //        db1.SaveChanges();
                //        return RedirectToAction("show", new { controller = "pdfs1", action = "show" });
                //    }
                //    else
                //    {
                //        ModelState.AddModelError("", "اطلاعات خودرا به درستی وارد نمایید");
                //        return View(user1);
                //    }
                //}
                //else
                //{
                //    ModelState.AddModelError("", "اطلاعات خودرا به درستی وارد نمایید");
                //    return View(user1);
                //}
            }
            ModelState.AddModelError("", "اطلاعات خودرا به درستی وارد نمایید");
            return View(user1);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
