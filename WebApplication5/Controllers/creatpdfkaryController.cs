﻿using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace WebApplication5.Controllers
{
    public class creatpdfkaryController : ApiController
    {
        // GET: api/creatpdfkary
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/creatpdfkary/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/creatpdfkary
        public HttpResponseMessage Post(string id,int rez)
        {
            StiReport report = new StiReport();
            string Path = HttpContext.Current.Server.MapPath("~/App_Data/stimul/1.mrt");
            report.Load(Path);
            report["@id"] = id;
            report["@rez"] = rez;
            report.Compile();
            report.Render();
            StiPdfExportSettings pdfSettings = new StiPdfExportSettings();
            string j = HttpContext.Current.Server.MapPath("~/pdf/" + id + ".Pdf");
            if (File.Exists(j))
            {
                File.Delete(j);
            }
            report.ExportDocument(StiExportFormat.Pdf, j, pdfSettings);


            string fileName = id + ".Pdf";
            string localFilePath;

            localFilePath = HttpContext.Current.Server.MapPath("~/pdf/" + id + ".Pdf");

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            return response;


           // return j;
        }

        // PUT: api/creatpdfkary/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/creatpdfkary/5
        public void Delete(int id)
        {
        }
    }
}
