﻿using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Results;

namespace WebApplication5.Controllers
{
    public class creatpdftahsilController : ApiController
    {
        // GET: api/creatpdftahsil
        public HttpResponseMessage Get()
        {

            string fileName="1";
            string localFilePath;

            localFilePath = HttpContext.Current.Server.MapPath("~/pdf/1.Pdf");

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

            return response;




        //    HttpResponseMessage result = null;
        
            //// sendo file to client
        //    byte[] bytes = Convert.FromBase64String(file.pdfBase64);


        //    result = Request.CreateResponse(HttpStatusCode.OK);
        //    result.Content = new ByteArrayContent(bytes);
        //    result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
        //    result.Content.Headers.ContentDisposition.FileName = file.name + ".pdf";
        

        //return result;
        }

        // GET: api/creatpdftahsil/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/creatpdftahsil
        public HttpResponseMessage Post(string id,int rez)
        {
            StiReport report = new StiReport();
            string Path = HttpContext.Current.Server.MapPath("~/App_Data/stimul/3.mrt");
            report.Load(Path);
            report["@id"] = id;
            report["@rez"] = rez;
            report.Compile();
            report.Render();
            StiPdfExportSettings pdfSettings = new StiPdfExportSettings();
            string j = HttpContext.Current.Server.MapPath("~/pdf/" + id + ".Pdf");
            if (File.Exists(j))
            {
                File.Delete(j);
            }
            report.ExportDocument(StiExportFormat.Pdf, j, pdfSettings);

            string fileName = id+".Pdf";
            string localFilePath;

            localFilePath = HttpContext.Current.Server.MapPath("~/pdf/"+id+".Pdf");

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

            return response;


            //return j;
        }

        // PUT: api/creatpdftahsil/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/creatpdftahsil/5
        public void Delete(int id)
        {
        }
    }
}
