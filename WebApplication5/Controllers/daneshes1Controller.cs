﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class daneshes1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: daneshes1
        public ActionResult Index(string userid, int rezkary, int reztahsil)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (rezkary != 0)
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = reztahsil;
                return View(db.daneshes.Where(u => u.usersid == userid && u.rezkary == rezkary).ToList());
            }
            else
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = reztahsil;
                return View(db.daneshes.Where(u => u.usersid == userid && u.reztahsil == reztahsil).ToList());
            }
        }

        // GET: daneshes1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            danesh danesh = db.daneshes.Find(id);
            if (danesh == null)
            {
                return HttpNotFound();
            }
            return View(danesh);
        }

        // GET: tahsils1/Create
        public ActionResult Create(int? rezkary, int? reztahsil, int? flag)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (flag == 0)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary + 1;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                    else
                    {
                        ViewBag.rezkary = 0;
                        ViewBag.reztahsil = reztahsil + 1;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 1)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                    else
                    {
                        ViewBag.rezkary = 0;
                        ViewBag.reztahsil = reztahsil;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 2)
                {

                    ViewBag.rezkary = rezkary + 1;
                    ViewBag.reztahsil = 0;
                    ViewBag.flag = flag;
                }
                else if (flag == 3)
                {

                    ViewBag.rezkary = 0;
                    ViewBag.reztahsil = reztahsil + 1;
                    ViewBag.flag = flag;
                }
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // POST: tahsils1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,hoze,mizan,madrak,rezkary,reztahsil")] danesh danesh)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.Where(g => g.Email == s).FirstOrDefault();
                    if (danesh.rezkary != 0 && danesh.rezkary > user.tedadrezkary)
                    {
                        user.tedadrezkary = danesh.rezkary;
                        pdf p = new pdf();
                        p.pdfname = "رزومه کاری" + danesh.rezkary.ToString();
                        p.rezkary = danesh.rezkary;
                        p.reztahsil = 0;
                        p.usersid = user.Id;
                        db.pdfs.Add(p);
                        danesh.usersid = user.Id;
                        db.daneshes.Add(danesh);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = danesh.rezkary, flag = 1 });
                    }
                    else if (danesh.rezkary != 0 && danesh.rezkary == user.tedadrezkary)
                    {
                        danesh.usersid = user.Id;
                        db.daneshes.Add(danesh);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = danesh.rezkary, flag = 1 });
                    }
                    else if (danesh.reztahsil != 0 && danesh.reztahsil > user.tedadreztahsil)
                    {
                        user.tedadreztahsil = danesh.reztahsil;
                        pdf p = new pdf();
                        p.pdfname = "رزومه تحصیلی" + danesh.reztahsil.ToString();
                        p.reztahsil = danesh.reztahsil;
                        p.usersid = user.Id;
                        p.rezkary = 0;
                        db.pdfs.Add(p);
                        danesh.usersid = user.Id;
                        db.daneshes.Add(danesh);
                        db.SaveChanges();
                        return RedirectToAction("newtahsil", new { controller = "pdfs1", action = "newtahsil", rez = danesh.reztahsil, flag = 1 });
                    }
                    else if (danesh.reztahsil != 0 && danesh.reztahsil == user.tedadreztahsil)
                    {
                        danesh.usersid = user.Id;
                        db.daneshes.Add(danesh);
                        db.SaveChanges();
                        return RedirectToAction("newtahsil", new { controller = "pdfs1", action = "newtahsil", rez = danesh.reztahsil, flag = 1 });
                    }
                }
                return View(danesh);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        public ActionResult newdanesh(int rezkary, int reztahsil)
        {
            if (rezkary != 0)
            {
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = 0;
            }
            else
            {
                ViewBag.rezkary = 0;
                ViewBag.reztahsil = reztahsil;
            }
            return View();
        }
        [HttpPost]
        public ActionResult newdanesh([Bind(Include = "id,usersid,hoze,mizan,madrak,rezkary,reztahsil")] danesh danesh)
        {

            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    db.Users.Where(g => g.Email == s).Load();
                    danesh.usersid = db.Users.Local[0].Id;
                    db.daneshes.Add(danesh);
                    db.SaveChanges();
                    return RedirectToAction("Index", null, new { controller = "daneshes1", action = "Index", userid = db.Users.Local[0].Id, rezkary = danesh.rezkary, reztahsil = danesh.reztahsil });
                }
                return View(danesh);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        // GET: tahsils1/Edit/5
        public ActionResult Edit(int? id)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            danesh danesh = db.daneshes.Find(id);
            if (danesh == null)
            {
                return HttpNotFound();
            }
            return View(danesh);
        }

        // POST: daneshes1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,hoze,mizan,madrak,rezkary,reztahsil")] danesh danesh)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                db.Entry(danesh).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", null, new { controller = "daneshes1", action = "Index", userid = db.Users.Local[0].Id, rezkary = danesh.rezkary, reztahsil = danesh.reztahsil });
            }
            return View(danesh);
        }

        // GET: daneshes1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            danesh danesh = db.daneshes.Find(id);
            if (danesh == null)
            {
                return HttpNotFound();
            }
            return View(danesh);
        }

        // POST: daneshes1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            danesh danesh = db.daneshes.Find(id);
            db.daneshes.Remove(danesh);
            db.SaveChanges();
            return RedirectToAction("Index", null, new { controller = "daneshes1", action = "Index", userid = db.Users.Local[0].Id, rezkary = danesh.rezkary, reztahsil = danesh.reztahsil });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
