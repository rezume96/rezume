﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class daneshesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/daneshes
        public IQueryable<danesh> Getdaneshes()
        {
            return db.daneshes;
        }

        // GET: api/daneshes/5
        [ResponseType(typeof(danesh))]
        public IHttpActionResult Getdanesh(int id)
        {
            danesh danesh = db.daneshes.Find(id);
            if (danesh == null)
            {
                return NotFound();
            }

            return Ok(danesh);
        }

        // PUT: api/daneshes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putdanesh(int id, danesh danesh)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != danesh.id)
            {
                return BadRequest();
            }

            db.Entry(danesh).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!daneshExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/daneshes
        [ResponseType(typeof(danesh))]
        public IHttpActionResult Postdanesh(danesh danesh)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.daneshes.Add(danesh);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = danesh.id }, danesh);
        }

        // DELETE: api/daneshes/5
        [ResponseType(typeof(danesh))]
        public IHttpActionResult Deletedanesh(int id)
        {
            danesh danesh = db.daneshes.Find(id);
            if (danesh == null)
            {
                return NotFound();
            }

            db.daneshes.Remove(danesh);
            db.SaveChanges();

            return Ok(danesh);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool daneshExists(int id)
        {
            return db.daneshes.Count(e => e.id == id) > 0;
        }
    }
}