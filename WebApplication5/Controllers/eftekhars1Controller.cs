﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class eftekhars1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: eftekhars1
        public ActionResult Index(string userid, int rezkary, int reztahsil)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (rezkary != 0)
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = reztahsil;
                return View(db.eftekhars.Where(u => u.usersid == userid && u.rezkary == rezkary).ToList());
            }
            else
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = reztahsil;
                return View(db.eftekhars.Where(u => u.usersid == userid && u.reztahsil == reztahsil).ToList());
            }
        }

        // GET: eftekhars1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eftekhar eftekhar = db.eftekhars.Find(id);
            if (eftekhar == null)
            {
                return HttpNotFound();
            }
            return View(eftekhar);
        }

        // GET: tahsils1/Create
        public ActionResult Create(int? rezkary, int? reztahsil, int? flag)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (flag == 0)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary + 1;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                    else
                    {
                        ViewBag.rezkary = 0;
                        ViewBag.reztahsil = reztahsil + 1;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 1)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                    else
                    {
                        ViewBag.rezkary = 0;
                        ViewBag.reztahsil = reztahsil;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 2)
                {

                    ViewBag.rezkary = rezkary + 1;
                    ViewBag.reztahsil = 0;
                    ViewBag.flag = flag;
                }
                else if (flag == 3)
                {

                    ViewBag.rezkary = 0;
                    ViewBag.reztahsil = reztahsil + 1;
                    ViewBag.flag = flag;
                }
                ViewBag.ff = "ali";
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // POST: tahsils1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,sal,tozih,rezkary,reztahsil")] eftekhar eftekhar)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.Where(g => g.Email == s).FirstOrDefault();
                    if (eftekhar.rezkary != 0 && eftekhar.rezkary > user.tedadrezkary)
                    {
                        user.tedadrezkary = eftekhar.rezkary;
                        pdf p = new pdf();
                        p.pdfname = "رزومه کاری" + eftekhar.rezkary.ToString();
                        p.rezkary = eftekhar.rezkary;
                        p.reztahsil = 0;
                        p.usersid = user.Id;
                        db.pdfs.Add(p);
                        eftekhar.usersid = user.Id;
                        db.eftekhars.Add(eftekhar);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = eftekhar.rezkary, flag = 1 });
                    }
                    else if (eftekhar.rezkary != 0 && eftekhar.rezkary == user.tedadrezkary)
                    {
                        eftekhar.usersid = user.Id;
                        db.eftekhars.Add(eftekhar);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = eftekhar.rezkary, flag = 1 });
                    }
                    else if (eftekhar.reztahsil != 0 && eftekhar.reztahsil > user.tedadreztahsil)
                    {
                        user.tedadreztahsil = eftekhar.reztahsil;
                        pdf p = new pdf();
                        p.pdfname = "رزومه تحصیلی" + eftekhar.reztahsil.ToString();
                        p.reztahsil = eftekhar.reztahsil;
                        p.usersid = user.Id;
                        p.rezkary = 0;
                        db.pdfs.Add(p);
                        eftekhar.usersid = user.Id;
                        db.eftekhars.Add(eftekhar);
                        db.SaveChanges();
                        return RedirectToAction("newtahsil", new { controller = "pdfs1", action = "newtahsil", rez = eftekhar.reztahsil, flag = 1 });
                    }
                    else if (eftekhar.reztahsil != 0 && eftekhar.reztahsil == user.tedadreztahsil)
                    {
                        eftekhar.usersid = user.Id;
                        db.eftekhars.Add(eftekhar);
                        db.SaveChanges();
                        return RedirectToAction("newtahsil", new { controller = "pdfs1", action = "newtahsil", rez = eftekhar.reztahsil, flag = 1 });
                    }
                }
                return View(eftekhar);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        public ActionResult neweftekhar(int rezkary, int reztahsil)
        {
            if (rezkary != 0)
            {
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = 0;
            }
            else
            {
                ViewBag.rezkary = 0;
                ViewBag.reztahsil = reztahsil;
            }
            return View();
        }
        [HttpPost]
        public ActionResult neweftekhar([Bind(Include = "id,usersid,sal,tozih,rezkary,reztahsil")] eftekhar eftekhar)
        {

            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    db.Users.Where(g => g.Email == s).Load();
                    eftekhar.usersid = db.Users.Local[0].Id;
                    db.eftekhars.Add(eftekhar);
                    db.SaveChanges();
                    return RedirectToAction("Index", null, new { controller = "eftekhars1", action = "Index", userid = db.Users.Local[0].Id, rezkary = eftekhar.rezkary, reztahsil = eftekhar.reztahsil });
                }
                return View(eftekhar);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        // GET: tahsils1/Edit/5
        public ActionResult Edit(int? id)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eftekhar eftekhar = db.eftekhars.Find(id);
            if (eftekhar == null)
            {
                return HttpNotFound();
            }
            return View(eftekhar);
        }

        // POST: eftekhars1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,sal,tozih,rezkary,reztahsil")] eftekhar eftekhar)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                db.Entry(eftekhar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", null, new { controller = "eftekhars1", action = "Index", userid = db.Users.Local[0].Id, rezkary = eftekhar.rezkary, reztahsil = eftekhar.reztahsil });
            }
            return View(eftekhar);
        }

        // GET: eftekhars1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            eftekhar eftekhar = db.eftekhars.Find(id);
            if (eftekhar == null)
            {
                return HttpNotFound();
            }
            return View(eftekhar);
        }

        // POST: eftekhars1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            eftekhar eftekhar = db.eftekhars.Find(id);
            db.eftekhars.Remove(eftekhar);
            db.SaveChanges();
            return RedirectToAction("Index", null, new { controller = "eftekhars1", action = "Index", userid = db.Users.Local[0].Id, rezkary = eftekhar.rezkary, reztahsil = eftekhar.reztahsil });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
