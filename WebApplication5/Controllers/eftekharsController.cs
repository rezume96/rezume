﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class eftekharsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/eftekhars
        public IQueryable<eftekhar> Geteftekhars()
        {
            return db.eftekhars;
        }

        // GET: api/eftekhars/5
        [ResponseType(typeof(eftekhar))]
        public IHttpActionResult Geteftekhar(int id)
        {
            eftekhar eftekhar = db.eftekhars.Find(id);
            if (eftekhar == null)
            {
                return NotFound();
            }

            return Ok(eftekhar);
        }

        // PUT: api/eftekhars/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puteftekhar(int id, eftekhar eftekhar)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eftekhar.id)
            {
                return BadRequest();
            }

            db.Entry(eftekhar).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!eftekharExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/eftekhars
        [ResponseType(typeof(eftekhar))]
        public IHttpActionResult Posteftekhar(eftekhar eftekhar)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.eftekhars.Add(eftekhar);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = eftekhar.id }, eftekhar);
        }

        // DELETE: api/eftekhars/5
        [ResponseType(typeof(eftekhar))]
        public IHttpActionResult Deleteeftekhar(int id)
        {
            eftekhar eftekhar = db.eftekhars.Find(id);
            if (eftekhar == null)
            {
                return NotFound();
            }

            db.eftekhars.Remove(eftekhar);
            db.SaveChanges();

            return Ok(eftekhar);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool eftekharExists(int id)
        {
            return db.eftekhars.Count(e => e.id == id) > 0;
        }
    }
}