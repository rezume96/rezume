﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class karygetController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public class test
        {
            public int rez { get; set; }
            public string email { get; set; }
        }
        // GET: api/kary1
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/kary1/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/kary1
        public IHttpActionResult Post(test test)
        {
            db.Users.Where(g => g.Email == test.email).Load();
            if (db.Users.Local.Count == 0)
            {
                string message1 = "BadRequest";
                return Json(new { message1 });
            }
            string usersid = db.Users.Local[0].Id;
            db.shoghlusers.Where(g => g.usersid == usersid && g.rez == test.rez).Load();
            db.tahsils.Where(g => g.usersid == usersid && g.rezkary == test.rez).Load();
            db.sabeghes.Where(g => g.usersid == usersid && g.rezkary == test.rez).Load();
            db.pishines.Where(g => g.usersid == usersid && g.rez == test.rez).Load();
            db.daneshes.Where(g => g.usersid == usersid && g.rezkary == test.rez).Load();
            db.projects.Where(g => g.usersid == usersid && g.rez == test.rez).Load();
            db.narms.Where(g => g.usersid == usersid && g.rez == test.rez).Load();
            db.zabanusers.Where(g => g.usersid == usersid && g.rezkary == test.rez).Load();
            db.eftekhars.Where(g => g.usersid == usersid && g.rezkary == test.rez).Load();



            List<Tuple<Tuple<usermodel, List<shoghlusermodel>, List<tahsilmodel>, List<sabeghemodel>, List<pishinemodel>, List<daneshmodel>, List<projectmodel>>,
                Tuple<List<narmmodel>, List<zabanusermodel>, List<eftekharmodel>>>> kol =
                new List<Tuple<Tuple<usermodel, List<shoghlusermodel>, List<tahsilmodel>, List<sabeghemodel>, List<pishinemodel>, List<daneshmodel>, List<projectmodel>>,
                Tuple<List<narmmodel>, List<zabanusermodel>, List<eftekharmodel>>>>();

            List<shoghlusermodel> shoghl = new List<shoghlusermodel>();
            List<tahsilmodel> tahsils = new List<Models.tahsilmodel>();
            List<sabeghemodel> sabeghes = new List<sabeghemodel>();
            List<pishinemodel> pishines = new List<pishinemodel>();
            List<daneshmodel> daneshs = new List<daneshmodel>();
            List<projectmodel> projects = new List<projectmodel>();
            List<narmmodel> narms = new List<narmmodel>();
            List<zabanusermodel> zaban = new List<zabanusermodel>();
            List<eftekharmodel> eftekhars = new List<eftekharmodel>();

            usermodel user = new usermodel();

            foreach (var item in db.Users.Local)
            {
                user.id = item.Id;
                user.Name = item.Name;
                user.pname = item.pname;
                user.shenasname = item.shenasname;
                user.D_tavalod = item.D_tavalod;
                user.mojarad = item.mojarad;
                user.jens = item.jens;
                user.khedmat = item.khedmat;
                user.Mobile = item.Mobile;
                user.Address = item.Address;
                user.shahr = item.shahr;
                user.eshteghal = item.eshteghal;
                user.pictur = item.pictur;
                user.tozih = item.tozih;
                user.tedadrezkary = item.tedadrezkary;
                user.tedadreztahsil = item.tedadreztahsil;
                user.email = item.Email;
                user.phon = item.PhoneNumber;

            }
            modelfactory model = new modelfactory();
            var m = db.shoghlusers.Local.Select(model.shoghluser);
            foreach (var item in db.shoghlusers.Local)
            {
                shoghl.Add(model.shoghluser(item));
            }
            foreach (var item in db.tahsils.Local)
            {
                tahsils.Add(model.tahsil(item));
            }
            foreach (var item in db.sabeghes.Local)
            {
                sabeghes.Add(model.sabeghe(item));
            }
            foreach (var item in db.pishines.Local)
            {
                pishines.Add(model.pishine(item));
            }
            foreach (var item in db.daneshes.Local)
            {
                daneshs.Add(model.danesh(item));
            }
            foreach (var item in db.projects.Local)
            {
                projects.Add(model.project(item));
            }

            foreach (var item in db.narms.Local)
            {
                narms.Add(model.narm(item));
            }

            foreach (var item in db.zabanusers.Local)
            {
                zaban.Add(model.zabanuser(item));
            }
            foreach (var item in db.eftekhars.Local)
            {
                eftekhars.Add(model.eftekhar(item));
            }
            Tuple<usermodel, List<shoghlusermodel>, List<tahsilmodel>, List<sabeghemodel>, List<pishinemodel>, List<daneshmodel>, List<projectmodel>> t1 =
                new Tuple<usermodel, List<shoghlusermodel>, List<tahsilmodel>, List<sabeghemodel>, List<pishinemodel>, List<daneshmodel>, List<projectmodel>>(user, shoghl, tahsils, sabeghes, pishines, daneshs, projects);
            Tuple<List<narmmodel>, List<zabanusermodel>, List<eftekharmodel>> t2 =
          new Tuple<List<narmmodel>, List<zabanusermodel>, List<eftekharmodel>>(narms, zaban, eftekhars);
            kol.Add(new Tuple<Tuple<usermodel, List<shoghlusermodel>, List<tahsilmodel>, List<sabeghemodel>, List<pishinemodel>, List<daneshmodel>, List<projectmodel>>,
                Tuple<List<narmmodel>, List<zabanusermodel>, List<eftekharmodel>>>(t1, t2));

            return Ok(kol);
        }

        // PUT: api/kary1/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/kary1/5
        public void Delete(int id)
        {
        }
    }
}
