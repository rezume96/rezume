﻿using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class karypostController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
       
        public IHttpActionResult Postkary(List<Tuple<Tuple<ApplicationUser, List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>, List<danesh>, List<project>>,
               Tuple<List<narm>, List<zabanuser>, List<eftekhar>>>> kol)
        {
            //////////////جدا کردن تمام اطلاعات ارسالی و فرستادن به کنترلر هر جدول
            foreach (var item in kol)
            {
                try
                {
                    userController ouser = new userController();
                    IHttpActionResult x = ouser.Get(item.Item1.Item1);
                    var j = x.GetType();
                    if (j.Name == "BadRequestResult")
                    {
                        string message = "BadRequest";
                        return Json(new { message });
                    }
                    else if (j.Name == "InternalServerErrorResult")
                    {

                        string message = "InternalServerError";
                        return Json(new { message });
                    }
                }
                catch (Exception)
                {
                    string message = "InternalServerError";
                    return Json(new { message });
                }
                try
                {
                    shoghlusersController shoghl = new shoghlusersController();
                    foreach (var item1 in item.Item1.Item2)
                    {
                        shoghl.Postshoghluser(item1);
                    }

                    tahsilsController tahsil = new tahsilsController();
                    foreach (var item1 in item.Item1.Item3)
                    {
                        tahsil.Posttahsil(item1);
                    }


                    sabeghesController sabeghe = new sabeghesController();
                    foreach (var item1 in item.Item1.Item4)
                    {
                        sabeghe.Postsabeghe(item1);
                    }

                    pishinesController pishine = new pishinesController();
                    foreach (var item5 in item.Item1.Item5)
                    {
                        pishine.Postpishine(item5);
                    }

                    daneshesController danesh = new daneshesController();
                    foreach (var item6 in item.Item1.Item6)
                    {
                        danesh.Postdanesh(item6);
                    }

                    projectsController proj = new projectsController();
                    foreach (var item7 in item.Item1.Item7)
                    {
                        proj.Postproject(item7);
                    }

                    narmsController narm = new narmsController();
                    foreach (var item8 in item.Item2.Item1)
                    {
                        narm.Postnarm(item8);
                    }

                    zabanusersController zaban = new zabanusersController();
                    foreach (var item2 in item.Item2.Item2)
                    {
                        zaban.Postzabanuser(item2);
                    }



                    eftekharsController eft = new eftekharsController();
                    foreach (var item8 in item.Item2.Item3)
                    {
                        eft.Posteftekhar(item8);
                    }

                    pdfsController opdf = new pdfsController();
                    pdf op = new Models.pdf();
                    op.pdfname = "رزومه کاری" + item.Item1.Item1.tedadrezkary;
                    op.rezkary = item.Item1.Item1.tedadrezkary;
                    op.usersid = item.Item1.Item1.Id;
                    op.reztahsil = 0;
                    opdf.Postpdf(op);



                    creatpdfkaryController j = new Controllers.creatpdfkaryController();
                    j.Post(item.Item1.Item1.Id, item.Item1.Item1.tedadrezkary);
                    var m = "ok";
                    return Json(new {m });
            }
                catch (Exception)
            {
                /////////////////////////////////////////////////////////////////////////////////////////////////////

                string message = "InternalServerError";
                return Json(new { message });
            }
        }
            return Ok();
        }
        
    }
}
