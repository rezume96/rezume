﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class karyputController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: api/karyput
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/karyput/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/karyput
        public IHttpActionResult Post(List<Tuple<Tuple<ApplicationUser, List<shoghluser>, List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>,
            List<danesh>>,Tuple<List<project>, List<narm>, List<zabanuser>, List<eftekhar>>>> kol)
        {
            //////////////ذخیره اطلاعات شخصی
            foreach (var item in kol)
            {
                try
                {
                    userController ouser = new userController();
                    IHttpActionResult x = ouser.Get(item.Item1.Item1);
                    var j = x.GetType();
                    if (j.Name == "BadRequestResult")
                    {
                        string message = "BadRequestResult";
                        return Json(new { message });
                    }
                    else if (j.Name == "InternalServerErrorResult")
                    {

                        string message = "InternalServerError";
                        return Json(new { message });
                    }
                }
                catch (Exception)
                {
                    string message = "InternalServerError";
                    return Json(new { message });
                }

                ///////ذخیره دیگر اطلاعات
                try
                {

                    /////////یا حذف داریم یا اضافه!!!!!!!!!!!!!!!!!!!!!!
                    //int rezkary = Convert.ToInt32(rez);
                    shoghlusersController shoghl = new shoghlusersController();
                    foreach (var item8 in item.Item1.Item2)
                    {
                        shoghl.Postshoghluser(item8);
                    }

                    foreach (var item8 in item.Item1.Item3)
                    {
                        int id = item8.id;
                        shoghl.Deleteshoghluser(id);
                    }



                    tahsilsController tah = new tahsilsController();
                    foreach (var item8 in item.Item1.Item4)
                    {
                        tah.Puttahsil(item8.id, item8);
                    }


                    sabeghesController sab = new sabeghesController();
                    foreach (var item8 in item.Item1.Item5)
                    {
                        sab.Putsabeghe(item8.id, item8);
                    }

                    pishinesController pish = new pishinesController();
                    foreach (var item8 in item.Item1.Item6)
                    {
                        pish.Putpishine(item8.id, item8);
                    }


                    daneshesController danesh = new daneshesController();
                    foreach (var item8 in item.Item1.Item7)
                    {
                        danesh.Putdanesh(item8.id, item8);
                    }


                    projectsController proj = new projectsController();
                    foreach (var item8 in item.Item2.Item1)
                    {
                        proj.Putproject(item8.id, item8);
                    }


                    narmsController narm = new narmsController();
                    foreach (var item8 in item.Item2.Item2)
                    {
                        narm.Putnarm(item8.id, item8);
                    }

                    zabanusersController zab = new zabanusersController();
                    foreach (var item8 in item.Item2.Item3)
                    {
                        zab.Putzabanuser(item8.id, item8);
                    }


                    eftekharsController eft = new eftekharsController();
                    foreach (var item8 in item.Item2.Item4)
                    {
                        eft.Puteftekhar(item8.id, item8);
                    }

                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "ok";
                    return Json(new { message });
                }
                catch (Exception)
                {
                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "InternalServerError";
                    return Json(new { message });
                }
            }
            return Ok();
        }

        // PUT: api/karyput/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/karyput/5
        public void Delete(int id)
        {
        }
    }
}
