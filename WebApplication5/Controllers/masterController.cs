﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class masterController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: api/master

        ////////گرفتن ای دی کاربر از متد لوگین بعد از ورود به سیستم



        public IHttpActionResult Get()
        {
            /////////////ارسال جدول شغل و شغل گروپ و زبان 
            try
            {
                db.shoghlgrops.Load();
                db.shoghls.Load();
                db.zabans.Load();
                List<Tuple<List<shoghlgrop>, List<shoghl>, List<zaban>>> listkol = new List<Tuple<List<shoghlgrop>, List<shoghl>, List<zaban>>>();
                List<shoghlgrop> sh = new List<shoghlgrop>();
                List<shoghl> shogl = new List<shoghl>();
                List<zaban> zaban = new List<Models.zaban>();
                foreach (var item in db.shoghlgrops.Local)
                {
                    sh.Add(new shoghlgrop { id = item.id, name = item.name });
                }
                foreach (var item in db.shoghls.Local)
                {
                    shogl.Add(new shoghl { id = item.id, name = item.name });
                }
                foreach (var item in db.zabans.Local)
                {
                    zaban.Add(new zaban { id = item.id, name = item.name });
                }
                listkol.Add(new Tuple<List<shoghlgrop>, List<shoghl>, List<zaban>>(sh, shogl, zaban));
                string message = "ok";
                return Json(new { message, listkol });
            }
            catch (Exception)
            {
                string message = "InternalServerError";
                return Json(new { message });
            }
        }


        /////////////////////////////    پست , پوت رزومه کاری



        #region kary

        public IHttpActionResult karry(string email, int rez)
        {
            db.Users.Where(g => g.Email == email).Load();
            if (db.Users.Local.Count == 0)
            {
                return NotFound();
            }
            string usersid = db.Users.Local[0].Id;
            db.shoghlusers.Where(g => g.usersid == usersid && g.rez == rez).Load();
            db.tahsils.Where(g => g.usersid == usersid && g.rezkary == rez).Load();
            db.sabeghes.Where(g => g.usersid == usersid && g.rezkary == rez).Load();
            db.pishines.Where(g => g.usersid == usersid && g.rez == rez).Load();
            db.daneshes.Where(g => g.usersid == usersid && g.rezkary == rez).Load();
            db.projects.Where(g => g.usersid == usersid && g.rez == rez).Load();
            db.narms.Where(g => g.usersid == usersid && g.rez == rez).Load();
            db.zabanusers.Where(g => g.usersid == usersid && g.rezkary == rez).Load();
            db.eftekhars.Where(g => g.usersid == usersid && g.rezkary == rez).Load();

            List<Tuple<Tuple<ApplicationUser, List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>, List<danesh>, List<project>>,
                Tuple<List<narm>, List<zabanuser>, List<eftekhar>>>> kol =
                new List<Tuple<Tuple<ApplicationUser, List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>, List<danesh>, List<project>>,
                Tuple<List<narm>, List<zabanuser>, List<eftekhar>>>>();

            List<shoghluser> shoghl = new List<shoghluser>();
            List<tahsil> tahsils = new List<Models.tahsil>();
            List<sabeghe> sabeghes = new List<sabeghe>();
            List<pishine> pishines = new List<pishine>();
            List<danesh> daneshs = new List<danesh>();
            List<project> projects = new List<project>();

            List<narm> narms = new List<narm>();
            List<zabanuser> zaban = new List<zabanuser>();
            List<eftekhar> eftekhars = new List<eftekhar>();

            ApplicationUser user = new ApplicationUser();

            foreach (var item in db.Users.Local)
            {
                user.Id = item.Id;
                user.Name = item.Name;
                user.pname = item.pname;
                user.shenasname = item.shenasname;
                user.D_tavalod = item.D_tavalod;
                user.mojarad = item.mojarad;
                user.jens = item.jens;
                user.khedmat = item.khedmat;
                user.Mobile = item.Mobile;
                user.Address = item.Address;
                user.shahr = item.shahr;
                user.eshteghal = item.eshteghal;
                user.pictur = item.pictur;
                user.tozih = item.tozih;
                user.tedadrezkary = item.tedadrezkary;
                user.tedadreztahsil = item.tedadreztahsil;
                user.Email = item.Email;
                user.PhoneNumber = item.PhoneNumber;

            }
            foreach (var item in db.shoghlusers.Local)
            {
                shoghl.Add(item);
            }
            foreach (var item in db.tahsils.Local)
            {
                tahsils.Add(item);
            }
            foreach (var item in db.sabeghes.Local)
            {
                sabeghes.Add(item);
            }
            foreach (var item in db.pishines.Local)
            {
                pishines.Add(item);
            }
            foreach (var item in db.daneshes.Local)
            {
                daneshs.Add(item);
            }
            foreach (var item in db.projects.Local)
            {
                projects.Add(item);
            }

            foreach (var item in db.narms.Local)
            {
                narms.Add(item);
            }

            foreach (var item in db.zabanusers.Local)
            {
                zaban.Add(item);
            }
            foreach (var item in db.eftekhars.Local)
            {
                eftekhars.Add(item);
            }
            Tuple<ApplicationUser, List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>, List<danesh>, List<project>> t1 =
                new Tuple<ApplicationUser, List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>, List<danesh>, List<project>>(user, shoghl, tahsils, sabeghes, pishines, daneshs, projects);
            Tuple<List<narm>, List<zabanuser>, List<eftekhar>> t2 =
          new Tuple<List<narm>, List<zabanuser>, List<eftekhar>>(narms, zaban, eftekhars);
            kol.Add(new Tuple<Tuple<ApplicationUser, List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>, List<danesh>, List<project>>, Tuple<List<narm>, List<zabanuser>, List<eftekhar>>>(t1, t2));
            
            string message = "ok";
            return Json(new { message,kol });
        }


        // POST: api/master

        //رزومه کاری
       
        // PUT: api/master/5
        //رزومه کاری
        public IHttpActionResult Putkary(/*string usersid, string rez, string[] shoghluserp1, string[] shoghluserd1,*/
            List<Tuple<Tuple<ApplicationUser, List<shoghluser>, List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>, List<danesh>>,
                Tuple<List<project>,List<narm>, List<zabanuser>, List<eftekhar>>>> kol)
        {
            //////////////ذخیره اطلاعات شخصی
            foreach (var item in kol)
            {
                try
                {
                    userController ouser = new userController();
                    IHttpActionResult x = ouser.Get(item.Item1.Item1);
                    var j = x.GetType();
                    if (j.Name == "BadRequestResult")
                    {
                        string message = "BadRequestResult";
                        return Json(new { message });
                    }
                    else if (j.Name == "InternalServerErrorResult")
                    {

                        string message = "InternalServerError";
                        return Json(new { message });
                    }
                }
                catch (Exception)
                {
                    string message = "InternalServerError";
                    return Json(new { message });
                }

                ///////ذخیره دیگر اطلاعات
                try
                {

                    /////////یا حذف داریم یا اضافه!!!!!!!!!!!!!!!!!!!!!!
                    //int rezkary = Convert.ToInt32(rez);
                    shoghlusersController shoghl = new shoghlusersController();
                    foreach (var item8 in item.Item1.Item2)
                    {
                        shoghl.Postshoghluser(item8);
                    }

                    foreach (var item8 in item.Item1.Item3)
                    {
                        int id = item8.id;
                        shoghl.Deleteshoghluser(id);
                    }



                    tahsilsController tah = new tahsilsController();
                    foreach (var item8 in item.Item1.Item4)
                    {
                        tah.Puttahsil(item8.id, item8);
                    }


                    sabeghesController sab = new sabeghesController();
                    foreach (var item8 in item.Item1.Item5)
                    {
                        sab.Putsabeghe(item8.id, item8);
                    }

                    pishinesController pish = new pishinesController();
                    foreach (var item8 in item.Item1.Item6)
                    {
                        pish.Putpishine(item8.id, item8);
                    }


                    daneshesController danesh = new daneshesController();
                    foreach (var item8 in item.Item1.Item7)
                    {
                        danesh.Putdanesh(item8.id, item8);
                    }


                    projectsController proj = new projectsController();
                    foreach (var item8 in item.Item2.Item1)
                    {
                        proj.Putproject(item8.id, item8);
                    }


                    narmsController narm = new narmsController();
                    foreach (var item8 in item.Item2.Item2)
                    {
                        narm.Putnarm(item8.id, item8);
                    }

                    zabanusersController zab = new zabanusersController();
                    foreach (var item8 in item.Item2.Item3)
                    {
                        zab.Putzabanuser(item8.id, item8);
                    }


                    eftekharsController eft = new eftekharsController();
                    foreach (var item8 in item.Item2.Item4)
                    {
                        eft.Puteftekhar(item8.id, item8);
                    }
                    pdfsController opdf = new pdfsController();
                    pdf op = new Models.pdf();
                    op.pdfname = "رزومه کاری" + item.Item1.Item1.tedadrezkary;
                    op.rezkary = item.Item1.Item1.tedadrezkary;
                    op.usersid = item.Item1.Item1.Id;
                    op.reztahsil = 0;
                    opdf.Postpdf(op);

                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "ok";
                    return Json(new { message });
                }
                catch (Exception)
                {
                    /////////////////////////////////////////////////////////////////////////////////////////////////////
                    
                    string message = "InternalServerError";
                    return Json(new { message });
                }
            }
            return Ok();
        }

        #endregion kary



        /////////////////////////////    پست , پوت رزومه تحصیلی


        #region tahsil


        public IHttpActionResult Gettahsil(string usersid, int rez)
        {
            try
            {

                db.Users.Where(g => g.Id == usersid).Load();
                if (db.Users.Local.Count == 0)
                {
                    return NotFound();
                }
                db.tahsils.Where(g => g.usersid == usersid && g.reztahsil == rez).Load();
                db.sabeghes.Where(g => g.usersid == usersid && g.reztahsil == rez).Load();
                db.daneshes.Where(g => g.usersid == usersid && g.reztahsil == rez).Load();
                db.zabanusers.Where(g => g.usersid == usersid && g.reztahsil == rez).Load();
                db.eftekhars.Where(g => g.usersid == usersid && g.reztahsil == rez).Load();


                List<Tuple<ApplicationUser, List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>>> kol =
                    new List<Tuple<ApplicationUser, List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>>>();


                ApplicationUser user = new ApplicationUser();
                List<tahsil> tahsils = new List<Models.tahsil>();
                List<sabeghe> sabeghes = new List<sabeghe>();
                List<danesh> daneshs = new List<danesh>();
                List<zabanuser> zaban = new List<zabanuser>();
                List<eftekhar> eftekhars = new List<eftekhar>();

                foreach (var item in db.Users.Local)
                {
                    user.Id = item.Id;
                    user.Name = item.Name;
                    user.pname = item.pname;
                    user.shenasname = item.shenasname;
                    user.D_tavalod = item.D_tavalod;
                    user.mojarad = item.mojarad;
                    user.jens = item.jens;
                    user.khedmat = item.khedmat;
                    user.Mobile = item.Mobile;
                    user.Address = item.Address;
                    user.shahr = item.shahr;
                    user.eshteghal = item.eshteghal;
                    user.pictur = item.pictur;
                    user.tozih = item.tozih;
                    user.tedadrezkary = item.tedadrezkary;
                    user.tedadreztahsil = item.tedadreztahsil;
                    user.Email = item.Email;
                    user.PhoneNumber = item.PhoneNumber;

                }
                foreach (var item in db.tahsils.Local)
                {
                    tahsils.Add(item);
                }
                foreach (var item in db.sabeghes.Local)
                {
                    sabeghes.Add(item);
                }
                foreach (var item in db.daneshes.Local)
                {
                    daneshs.Add(item);
                }
                foreach (var item in db.zabanusers.Local)
                {
                    zaban.Add(item);
                }
                foreach (var item in db.eftekhars.Local)
                {
                    eftekhars.Add(item);
                }

                Tuple<ApplicationUser, List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>> t1 =
                   new Tuple<ApplicationUser, List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>>(user, tahsils, sabeghes, daneshs, zaban, eftekhars);
                kol.Add(t1);
                
                string message = "ok";
                return Json(new { message,kol });
            }
            catch (Exception)
            {
                string message = "InternalServerError";
                return Json(new { message });
            }
        }

        //روزمه تحصیلی
        public IHttpActionResult Posttahsil(List<Tuple<ApplicationUser, List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>>> kol)
        {
            foreach (var item in kol)
            {
                int rez1 = 0;
                try
                {
                    userController ouser = new userController();
                    int q = item.Item1.tedadreztahsil;
                    q++;
                    item.Item1.tedadrezkary = q;
                    IHttpActionResult x = ouser.Get(item.Item1);
                    var j = x.GetType();
                    if (j.Name == "OkResult")
                    {
                        rez1 = q;
                    }
                    else if (j.Name == "BadRequestResult")
                    {
                        string message = "BadRequestResult";
                        return Json(new { message });
                    }
                    else if (j.Name == "InternalServerErrorResult")
                    {

                        string message = "InternalServerError";
                        return Json(new { message });
                    }
                }
                catch (Exception)
                {
                    string message = "InternalServerError";
                    return Json(new { message });
                }
                try
                {

                    tahsilsController tahsil = new tahsilsController();
                    foreach (var item1 in item.Item2)
                    {
                        tahsil.Posttahsil(item1);
                    }

                    sabeghesController sabeghe = new sabeghesController();
                    foreach (var item1 in item.Item3)
                    {
                        sabeghe.Postsabeghe(item1);
                    }

                    daneshesController danesh = new daneshesController();
                    foreach (var item6 in item.Item4)
                    {
                        danesh.Postdanesh(item6);
                    }

                    zabanusersController zaban = new zabanusersController();
                    foreach (var item2 in item.Item5)
                    {
                        zaban.Postzabanuser(item2);
                    }


                    eftekharsController eft = new eftekharsController();
                    foreach (var item8 in item.Item6)
                    {
                        eft.Posteftekhar(item8);
                    }

                    pdfsController opdf = new pdfsController();
                    pdf op = new Models.pdf();
                    op.pdfname = "رزومه کاری" + item.Item1.tedadreztahsil;
                    op.reztahsil = item.Item1.tedadreztahsil;
                    op.usersid = item.Item1.Id;
                    op.rezkary = 0;
                    opdf.Postpdf(op);

                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "ok";
                    return Json(new { message });
                }
                catch (Exception)
                {
                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "InternalServerError";
                    return Json(new { message });
                }
            }
            return Ok();
        }


        //روزمه تحصیلی

        public IHttpActionResult Puttahsil(List<Tuple<ApplicationUser, List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>>> kol)
        {
            foreach (var item in kol)
            {
                try
                {
                    userController ouser = new userController();
                    IHttpActionResult x = ouser.Get(item.Item1);
                    var j = x.GetType();
                    if (j.Name == "BadRequestResult")
                    {
                        string message = "BadRequestResult";
                        return Json(new { message });
                    }
                    else if (j.Name == "InternalServerErrorResult")
                    {

                        string message = "InternalServerError";
                        return Json(new { message });
                    }
                }
                catch (Exception)
                {
                    string message = "InternalServerError";
                    return Json(new { message });
                }

                ///////ذخیره دیگر اطلاعات
                try
                {


                    tahsilsController tah = new tahsilsController();
                    foreach (var item8 in item.Item2)
                    {
                        tah.Puttahsil(item8.id, item8);
                    }


                    sabeghesController sab = new sabeghesController();
                    foreach (var item8 in item.Item3)
                    {
                        sab.Putsabeghe(item8.id, item8);
                    }


                    daneshesController danesh = new daneshesController();
                    foreach (var item8 in item.Item4)
                    {
                        danesh.Putdanesh(item8.id, item8);
                    }
                    zabanusersController zab = new zabanusersController();
                    foreach (var item8 in item.Item5)
                    {
                        zab.Putzabanuser(item8.id, item8);
                    }
                    eftekharsController eft = new eftekharsController();
                    foreach (var item8 in item.Item6)
                    {
                        eft.Puteftekhar(item8.id, item8);
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "ok";
                    return Json(new { message });
                }
                catch (Exception)
                {
                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "InternalServerError";
                    return Json(new { message });
                }
            }
            return Ok();
        }


        #endregion tahsil


        // DELETE: api/master/5
        public IHttpActionResult Delete(string usersid, int rezkary, int reztahsil)
        {
            try
            {
                if (rezkary != 0)
                {
                    //List<shoghluser>, List<tahsil>, List<sabeghe>, List<pishine>, List<danesh>, List < project >>,
                    //Tuple < List<narm>, List<zabanuser>, List < eftekhar
                    shoghlusersController osh = new shoghlusersController();
                    tahsilsController ota = new tahsilsController();
                    sabeghesController osa = new sabeghesController();
                    pishinesController opi = new pishinesController();
                    daneshesController oda = new daneshesController();
                    projectsController opro = new projectsController();
                    narmsController ona = new narmsController();
                    zabanusersController oza = new zabanusersController();
                    eftekharsController oef = new eftekharsController();
                    pdfsController opdf = new pdfsController();

                    db.shoghlusers.Where(g => g.usersid == usersid && g.rez == rezkary).Load();
                    foreach (var item in db.shoghlusers.Local)
                    {
                        osh.Deleteshoghluser(item.id);
                    }
                    db.tahsils.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.tahsils.Local)
                    {
                        ota.Deletetahsil(item.id);
                    }
                    db.sabeghes.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.sabeghes.Local)
                    {
                        osa.Deletesabeghe(item.id);
                    }
                    db.pishines.Where(g => g.usersid == usersid && g.rez == rezkary).Load();
                    foreach (var item in db.pishines.Local)
                    {
                        opi.Deletepishine(item.id);
                    }
                    db.daneshes.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.daneshes.Local)
                    {
                        oda.Deletedanesh(item.id);
                    }
                    db.projects.Where(g => g.usersid == usersid && g.rez == rezkary).Load();
                    foreach (var item in db.projects.Local)
                    {
                        opro.Deleteproject(item.id);
                    }
                    db.narms.Where(g => g.usersid == usersid && g.rez == rezkary).Load();
                    foreach (var item in db.narms.Local)
                    {
                        ona.Deletenarm(item.id);
                    }
                    db.zabanusers.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.zabanusers.Local)
                    {
                        oza.Deletezabanuser(item.id);
                    }
                    db.eftekhars.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.eftekhars.Local)
                    {
                        oef.Deleteeftekhar(item.id);
                    }

                    string message = "ok";
                    return Json(new { message });
                }
                else
                {
                    //List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>
                    tahsilsController ota = new tahsilsController();
                    sabeghesController osa = new sabeghesController();
                    daneshesController oda = new daneshesController();
                    zabanusersController oza = new zabanusersController();
                    eftekharsController oef = new eftekharsController();
                    pdfsController opdf = new pdfsController();
                    
                    db.tahsils.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.tahsils.Local)
                    {
                        ota.Deletetahsil(item.id);
                    }
                    db.sabeghes.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.sabeghes.Local)
                    {
                        osa.Deletesabeghe(item.id);
                    }
                    db.daneshes.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.daneshes.Local)
                    {
                        oda.Deletedanesh(item.id);
                    }
                    db.zabanusers.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.zabanusers.Local)
                    {
                        oza.Deletezabanuser(item.id);
                    }
                    db.eftekhars.Where(g => g.usersid == usersid && g.rezkary == rezkary).Load();
                    foreach (var item in db.eftekhars.Local)
                    {
                        oef.Deleteeftekhar(item.id);
                    }

                    string message = "ok";
                    return Json(new { message });
                }
            }
            catch (Exception)
            {
                string message = "InternalServerError";
                return Json(new { message });
            }
            
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
