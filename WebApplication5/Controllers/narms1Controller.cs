﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class narms1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: narms1
        public ActionResult Index(string userid, int rezkary)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (rezkary != 0)
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                return View(db.narms.Where(u => u.usersid == userid && u.rez == rezkary).ToList());
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // GET: narms1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            narm narm = db.narms.Find(id);
            if (narm == null)
            {
                return HttpNotFound();
            }
            return View(narm);
        }

        // GET: tahsils1/Create
        public ActionResult Create(int? rezkary, int? flag)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (flag == 0)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary + 1;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 1)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 2)
                {

                    ViewBag.rezkary = rezkary + 1;
                    ViewBag.reztahsil = 0;
                    ViewBag.flag = flag;
                }
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // POST: tahsils1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,title,mizan,madrak,rez")] narm narm)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.Where(g => g.Email == s).FirstOrDefault();
                    if (narm.rez != 0 && narm.rez > user.tedadrezkary)
                    {
                        user.tedadrezkary = narm.rez;
                        pdf p = new pdf();
                        p.pdfname = "رزومه کاری" + narm.rez.ToString();
                        p.rezkary = narm.rez;
                        p.reztahsil = 0;
                        p.usersid = user.Id;
                        db.pdfs.Add(p);
                        narm.usersid = user.Id;
                        db.narms.Add(narm);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = narm.rez, flag = 1 });
                    }
                    else if (narm.rez != 0 && narm.rez == user.tedadrezkary)
                    {
                        narm.usersid = user.Id;
                        db.narms.Add(narm);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = narm.rez, flag = 1 });
                    }
                }
                return View(narm);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        public ActionResult newnarm(int rezkary)
        {
            if (rezkary != 0)
            {
                ViewBag.rezkary = rezkary;
            }
            return View();
        }
        [HttpPost]
        public ActionResult newnarm([Bind(Include = "id,usersid,title,mizan,madrak,rez")] narm narm)
        {

            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    db.Users.Where(g => g.Email == s).Load();
                    narm.usersid = db.Users.Local[0].Id;
                    db.narms.Add(narm);
                    db.SaveChanges();
                    return RedirectToAction("Index", null, new { controller = "narms1", action = "Index", userid = db.Users.Local[0].Id, rezkary = narm.rez });
                }
                return View(narm);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        // GET: tahsils1/Edit/5
        public ActionResult Edit(int? id)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            narm narm = db.narms.Find(id);
            if (narm == null)
            {
                return HttpNotFound();
            }
            return View(narm);
        }

        // POST: narms1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,title,mizan,madrak,rez")] narm narm)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                db.Entry(narm).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", null, new { controller = "narms1", action = "Index", userid = db.Users.Local[0].Id, rezkary = narm.rez });
            }
            return View(narm);
        }

        // GET: narms1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            narm narm = db.narms.Find(id);
            if (narm == null)
            {
                return HttpNotFound();
            }
            return View(narm);
        }

        // POST: narms1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            narm narm = db.narms.Find(id);
            db.narms.Remove(narm);
            db.SaveChanges();
            return RedirectToAction("Index", null, new { controller = "narms1", action = "Index", userid = db.Users.Local[0].Id, rezkary = narm.rez });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
