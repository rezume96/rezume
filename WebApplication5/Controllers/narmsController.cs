﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class narmsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/narms
        public IQueryable<narm> Getnarms()
        {
            return db.narms;
        }

        // GET: api/narms/5
        [ResponseType(typeof(narm))]
        public IHttpActionResult Getnarm(int id)
        {
            narm narm = db.narms.Find(id);
            if (narm == null)
            {
                return NotFound();
            }

            return Ok(narm);
        }

        // PUT: api/narms/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putnarm(int id, narm narm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != narm.id)
            {
                return BadRequest();
            }

            db.Entry(narm).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!narmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/narms
        [ResponseType(typeof(narm))]
        public IHttpActionResult Postnarm(narm narm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.narms.Add(narm);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = narm.id }, narm);
        }

        // DELETE: api/narms/5
        [ResponseType(typeof(narm))]
        public IHttpActionResult Deletenarm(int id)
        {
            narm narm = db.narms.Find(id);
            if (narm == null)
            {
                return NotFound();
            }

            db.narms.Remove(narm);
            db.SaveChanges();

            return Ok(narm);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool narmExists(int id)
        {
            return db.narms.Count(e => e.id == id) > 0;
        }
    }
}