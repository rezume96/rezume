﻿using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class pdfs1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: pdfs1
        public ActionResult Index()
        {
            checkController v = new checkController();
            string f = v.getcheck();
            if (f != "")
            {
                db.Users.Where(r => r.Email == f).Load();
                string id = db.Users.Local[0].Id;
                ViewBag.rezkary = db.Users.Local[0].tedadrezkary;
                ViewBag.reztahsil = db.Users.Local[0].tedadreztahsil;
                return View(db.pdfs.Where(j => j.usersid == id).ToList());
            }
            else
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
        }

        // GET: pdfs1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pdf pdf = db.pdfs.Find(id);
            if (pdf == null)
            {
                return HttpNotFound();
            }
            return View(pdf);
        }

        // GET: pdfs1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: pdfs1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,pdfname,rezkary,reztahsil")] pdf pdf)
        {
            if (ModelState.IsValid)
            {
                db.pdfs.Add(pdf);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pdf);
        }

        // GET: pdfs1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pdf pdf = db.pdfs.Find(id);
            if (pdf == null)
            {
                return HttpNotFound();
            }
            return View(pdf);
        }

        // POST: pdfs1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,pdfname,rezkary,reztahsil")] pdf pdf)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pdf).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pdf);
        }

        // GET: pdfs1/Delete/5
        public ActionResult Delete(string usersid, int? rezkary, int reztahsil)
        {
            if (rezkary == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pdf pdf = db.pdfs.Find(rezkary);
            if (pdf == null)
            {
                return HttpNotFound();
            }
            return View(pdf);
        }

        // POST: pdfs1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            pdf pdf = db.pdfs.Find(id);
            db.pdfs.Remove(pdf);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult newkary(int? rez, int? flag)
        {
            checkController v = new checkController();
            string f = v.getcheck();
            if (f != "")
            {
                ViewBag.rezkary = rez;
                ViewBag.reztahsil = 0;
                ViewBag.flag = flag;
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        public ActionResult newtahsil(int? rez, int? flag)
        {
            checkController v = new checkController();
            string f = v.getcheck();
            if (f != "")
            {
                ViewBag.rezkary = 0;
                ViewBag.reztahsil = rez;
                ViewBag.flag = flag;
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        public ActionResult Editrez(string userid,int rezkary,int reztahsil)
        {
            if (rezkary != 0)
            {
                return RedirectToAction("editkary",null,new { userid = userid, rez = rezkary });
            }
            else
            {
                return RedirectToAction("edittahsil", null, new { userid = userid, rez = reztahsil });
            }
        }
        public ActionResult editkary(string userid,int rez)
        {
            checkController v = new checkController();
            string f = v.getcheck();
            if (f != "")
            {
                ViewBag.rezkary = rez;
                ViewBag.reztahsil = 0;
                ViewBag.userid = userid;
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
             
        }
        public ActionResult edittahsil(string userid, int rez)
        {
            checkController v = new checkController();
            string f = v.getcheck();
            if (f != "")
            {
                ViewBag.rezkary = 0;
                ViewBag.reztahsil = rez;
                ViewBag.userid = userid;
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        public ActionResult show()
        {
            checkController c = new checkController();
            string f = c.getcheck();
            if (f == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            db.Users.Where(g => g.Email == f).Load();
            ViewBag.pro = db.Users.Local[0].profil;
            return View();
        }
        public ActionResult Simple()
        {
            return View();
        }
        public ActionResult FromLoadFileReport(string userid,int rezkary,int reztahsil)
        {
            if (rezkary != 0)
            {
                StiReport report = new StiReport();
                string Path = Server.MapPath("~/App_Data/stimul/1.mrt"); 
                report.Load(Path);
                report["@id"] = userid;
                report["@rez"] = rezkary;
                return StiMvcViewer.GetReportSnapshotResult(HttpContext, report);
            }
            else
            {
                StiReport report = new StiReport();
                
                string Path = Server.MapPath("~/App_Data/stimul/3.mrt");
                report.Load(Path);
                report["@id"] = userid;
                report["@rez"] = reztahsil;
                return StiMvcViewer.GetReportSnapshotResult(HttpContext, report);
            }
        }

        //ایجاد پرینت
        public ActionResult PrintReport()
        {
            return StiMvcViewer.PrintReportResult(this.HttpContext);
        }
        //ایجاد خروجی
        public ActionResult ExportReport()
        {
            return StiMvcViewer.ExportReportResult(this.HttpContext);
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult(this.HttpContext);
        }

        public ActionResult Interaction()
        {
            return StiMvcViewer.InteractionResult(this.HttpContext);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
