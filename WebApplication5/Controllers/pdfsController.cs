﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class pdfsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/pdfs
        public IQueryable<pdf> Getpdfs()
        {
            return db.pdfs;
        }

        // GET: api/pdfs/5
        [ResponseType(typeof(pdf))]
        public IHttpActionResult Getpdf(int id)
        {
            pdf pdf = db.pdfs.Find(id);
            if (pdf == null)
            {
                return NotFound();
            }

            return Ok(pdf);
        }

        // PUT: api/pdfs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putpdf(int id, pdf pdf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pdf.id)
            {
                return BadRequest();
            }

            db.Entry(pdf).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!pdfExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/pdfs
        [ResponseType(typeof(pdf))]
        public IHttpActionResult Postpdf(pdf pdf)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.pdfs.Add(pdf);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pdf.id }, pdf);
        }

        // DELETE: api/pdfs/5
        [ResponseType(typeof(pdf))]
        public IHttpActionResult Deletepdf(int id)
        {
            pdf pdf = db.pdfs.Find(id);
            if (pdf == null)
            {
                return NotFound();
            }

            db.pdfs.Remove(pdf);
            db.SaveChanges();

            return Ok(pdf);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool pdfExists(int id)
        {
            return db.pdfs.Count(e => e.id == id) > 0;
        }
    }
}