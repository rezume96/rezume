﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class pishines1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        

        // GET: tahsils1
        public ActionResult Index(string userid, int rezkary)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (rezkary != 0)
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                var pishines = db.pishines.Include(p => p.shoghlgrops).Where(u => u.usersid == userid && u.rez == rezkary);
                return View(pishines.ToList());
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }


        // GET: pishines1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pishine pishine = db.pishines.Find(id);
            if (pishine == null)
            {
                return HttpNotFound();
            }
            return View(pishine);
        }

        // GET: pishines1/Create
        public ActionResult Create(int? rezkary, int? flag)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (flag == 0)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary + 1;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 1)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 2)
                {

                    ViewBag.rezkary = rezkary + 1;
                    ViewBag.reztahsil = 0;
                    ViewBag.flag = flag;
                }
                ViewBag.shoghlgropid = new SelectList(db.shoghlgrops, "id", "name");
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // POST: tahsils1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,sazmanname,title,sath,shoro,payan,tozih,shoghlgropid,rez")] pishine pishine)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.Where(g => g.Email == s).FirstOrDefault();
                    if (pishine.rez != 0 && pishine.rez > user.tedadrezkary)
                    {
                        user.tedadrezkary = pishine.rez;
                        pdf p = new pdf();
                        p.pdfname = "رزومه کاری" + pishine.rez.ToString();
                        p.rezkary = pishine.rez;
                        p.reztahsil = 0;
                        p.usersid = user.Id;
                        db.pdfs.Add(p);
                        pishine.usersid = user.Id;
                        db.pishines.Add(pishine);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = pishine.rez, flag = 1 });
                    }
                    else if (pishine.rez != 0 && pishine.rez == user.tedadrezkary)
                    {
                        pishine.usersid = user.Id;
                        db.pishines.Add(pishine);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = pishine.rez, flag = 1 });
                    }
                }
                ViewBag.shoghlgropid = new SelectList(db.shoghlgrops, "id", "name", pishine.shoghlgropid);
                return View(pishine);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }


        public ActionResult newpishine(int rezkary)
        {
            if (rezkary != 0)
            {
                ViewBag.rezkary = rezkary;
            }
            ViewBag.shoghlgropid = new SelectList(db.shoghlgrops, "id", "name");
            return View();
        }
        [HttpPost]
        public ActionResult newpishine([Bind(Include = "id,usersid,sazmanname,title,sath,shoro,payan,tozih,shoghlgropid,rez")] pishine pishine)
        {

            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    db.Users.Where(g => g.Email == s).Load();
                    pishine.usersid = db.Users.Local[0].Id;
                    db.pishines.Add(pishine);
                    db.SaveChanges();
                    return RedirectToAction("Index", null, new { controller = "pishines1", action = "Index", userid = db.Users.Local[0].Id, rezkary = pishine.rez});
                }
                ViewBag.shoghlgropid = new SelectList(db.shoghlgrops, "id", "name", pishine.shoghlgropid);
                return View(pishine);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        // GET: tahsils1/Edit/5
        public ActionResult Edit(int? id)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pishine pishine = db.pishines.Find(id);
            if (pishine == null)
            {
                return HttpNotFound();
            }
            ViewBag.shoghlgropid = new SelectList(db.shoghlgrops, "id", "name", pishine.shoghlgropid);
            return View(pishine);
        }

        // POST: pishines1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,sazmanname,title,sath,shoro,payan,tozih,shoghlgropid,rez")] pishine pishine)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                db.Entry(pishine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", null, new { controller = "pishines1", action = "Index", userid = db.Users.Local[0].Id, rezkary = pishine.rez });
            }
            ViewBag.shoghlgropid = new SelectList(db.shoghlgrops, "id", "name", pishine.shoghlgropid);
            return View(pishine);
        }

        // GET: pishines1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            pishine pishine = db.pishines.Find(id);
            if (pishine == null)
            {
                return HttpNotFound();
            }
            return View(pishine);
        }

        // POST: pishines1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            pishine pishine = db.pishines.Find(id);
            db.pishines.Remove(pishine);
            db.SaveChanges();
            return RedirectToAction("Index", null, new { controller = "pishines1", action = "Index", userid = db.Users.Local[0].Id, rezkary = pishine.rez });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
