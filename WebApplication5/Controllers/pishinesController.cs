﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class pishinesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/pishines
        public IQueryable<pishine> Getpishines()
        {
            return db.pishines;
        }

        // GET: api/pishines/5
        [ResponseType(typeof(pishine))]
        public IHttpActionResult Getpishine(int id)
        {
            pishine pishine = db.pishines.Find(id);
            if (pishine == null)
            {
                return NotFound();
            }

            return Ok(pishine);
        }

        // PUT: api/pishines/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putpishine(int id, pishine pishine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != pishine.id)
            {
                return BadRequest();
            }

            db.Entry(pishine).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!pishineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/pishines
        [ResponseType(typeof(pishine))]
        public IHttpActionResult Postpishine(pishine pishine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.pishines.Add(pishine);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = pishine.id }, pishine);
        }

        // DELETE: api/pishines/5
        [ResponseType(typeof(pishine))]
        public IHttpActionResult Deletepishine(int id)
        {
            pishine pishine = db.pishines.Find(id);
            if (pishine == null)
            {
                return NotFound();
            }

            db.pishines.Remove(pishine);
            db.SaveChanges();

            return Ok(pishine);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool pishineExists(int id)
        {
            return db.pishines.Count(e => e.id == id) > 0;
        }
    }
}