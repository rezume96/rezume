﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class projects1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: narms1
        public ActionResult Index(string userid, int rezkary)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (rezkary != 0)
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                return View(db.projects.Where(u => u.usersid == userid && u.rez == rezkary).ToList());
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // GET: projects1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            project project = db.projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // GET: tahsils1/Create
        public ActionResult Create(int? rezkary, int? flag)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (flag == 0)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary + 1;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 1)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 2)
                {

                    ViewBag.rezkary = rezkary + 1;
                    ViewBag.reztahsil = 0;
                    ViewBag.flag = flag;
                }
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // POST: tahsils1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,title,semat,sal,tozih,rez")] project project)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.Where(g => g.Email == s).FirstOrDefault();
                    if (project.rez != 0 && project.rez > user.tedadrezkary)
                    {
                        user.tedadrezkary = project.rez;
                        pdf p = new pdf();
                        p.pdfname = "رزومه کاری" + project.rez.ToString();
                        p.rezkary = project.rez;
                        p.reztahsil = 0;
                        p.usersid = user.Id;
                        db.pdfs.Add(p);
                        project.usersid = user.Id;
                        db.projects.Add(project);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = project.rez, flag = 1 });
                    }
                    else if (project.rez != 0 && project.rez == user.tedadrezkary)
                    {
                        project.usersid = user.Id;
                        db.projects.Add(project);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = project.rez, flag = 1 });
                    }
                }
                return View(project);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        public ActionResult newproject(int rezkary)
        {
            if (rezkary != 0)
            {
                ViewBag.rezkary = rezkary;
            }
            return View();
        }
        [HttpPost]
        public ActionResult newproject([Bind(Include = "id,usersid,title,semat,sal,tozih,rez")] project project)
        {

            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    db.Users.Where(g => g.Email == s).Load();
                    project.usersid = db.Users.Local[0].Id;
                    db.projects.Add(project);
                    db.SaveChanges();
                    return RedirectToAction("Index", null, new { controller = "projects1", action = "Index", userid = db.Users.Local[0].Id, rezkary = project.rez });
                }
                return View(project);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        // GET: tahsils1/Edit/5
        public ActionResult Edit(int? id)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            project project = db.projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: projects1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,title,semat,sal,tozih,rez")] project project)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                db.Entry(project).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", null, new { controller = "projects1", action = "Index", userid = db.Users.Local[0].Id, rezkary = project.rez });
            }
            return View(project);
        }

        // GET: projects1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            project project = db.projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: projects1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            project project = db.projects.Find(id);
            db.projects.Remove(project);
            db.SaveChanges();
            return RedirectToAction("Index", null, new { controller = "projects1", action = "Index", userid = db.Users.Local[0].Id, rezkary = project.rez });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
