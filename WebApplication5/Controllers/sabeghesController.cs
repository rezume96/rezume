﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class sabeghesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/sabeghes
        public IQueryable<sabeghe> Getsabeghes()
        {
            return db.sabeghes;
        }

        // GET: api/sabeghes/5
        [ResponseType(typeof(sabeghe))]
        public IHttpActionResult Getsabeghe(int id)
        {
            sabeghe sabeghe = db.sabeghes.Find(id);
            if (sabeghe == null)
            {
                return NotFound();
            }

            return Ok(sabeghe);
        }

        // PUT: api/sabeghes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putsabeghe(int id, sabeghe sabeghe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sabeghe.id)
            {
                return BadRequest();
            }

            db.Entry(sabeghe).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!sabegheExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/sabeghes
        [ResponseType(typeof(sabeghe))]
        public IHttpActionResult Postsabeghe(sabeghe sabeghe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.sabeghes.Add(sabeghe);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = sabeghe.id }, sabeghe);
        }

        // DELETE: api/sabeghes/5
        [ResponseType(typeof(sabeghe))]
        public IHttpActionResult Deletesabeghe(int id)
        {
            sabeghe sabeghe = db.sabeghes.Find(id);
            if (sabeghe == null)
            {
                return NotFound();
            }

            db.sabeghes.Remove(sabeghe);
            db.SaveChanges();

            return Ok(sabeghe);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool sabegheExists(int id)
        {
            return db.sabeghes.Count(e => e.id == id) > 0;
        }
    }
}