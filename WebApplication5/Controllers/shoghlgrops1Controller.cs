﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class shoghlgrops1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: shoghlgrops1
        public ActionResult Index()
        {
            return View(db.shoghlgrops.ToList());
        }

        // GET: shoghlgrops1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghlgrop shoghlgrop = db.shoghlgrops.Find(id);
            if (shoghlgrop == null)
            {
                return HttpNotFound();
            }
            return View(shoghlgrop);
        }

        // GET: shoghlgrops1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: shoghlgrops1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name")] shoghlgrop shoghlgrop)
        {
            if (ModelState.IsValid)
            {
                db.shoghlgrops.Add(shoghlgrop);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(shoghlgrop);
        }

        // GET: shoghlgrops1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghlgrop shoghlgrop = db.shoghlgrops.Find(id);
            if (shoghlgrop == null)
            {
                return HttpNotFound();
            }
            return View(shoghlgrop);
        }

        // POST: shoghlgrops1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name")] shoghlgrop shoghlgrop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shoghlgrop).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shoghlgrop);
        }

        // GET: shoghlgrops1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghlgrop shoghlgrop = db.shoghlgrops.Find(id);
            if (shoghlgrop == null)
            {
                return HttpNotFound();
            }
            return View(shoghlgrop);
        }

        // POST: shoghlgrops1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            shoghlgrop shoghlgrop = db.shoghlgrops.Find(id);
            db.shoghlgrops.Remove(shoghlgrop);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
