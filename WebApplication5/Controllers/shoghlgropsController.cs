﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class shoghlgropsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/shoghlgrops
        public IQueryable<shoghlgrop> Getshoghlgrops()
        {
            return db.shoghlgrops;
        }

        // GET: api/shoghlgrops/5
        [ResponseType(typeof(shoghlgrop))]
        public IHttpActionResult Getshoghlgrop(int id)
        {
            shoghlgrop shoghlgrop = db.shoghlgrops.Find(id);
            if (shoghlgrop == null)
            {
                return NotFound();
            }

            return Ok(shoghlgrop);
        }

        // PUT: api/shoghlgrops/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putshoghlgrop(int id, shoghlgrop shoghlgrop)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shoghlgrop.id)
            {
                return BadRequest();
            }

            db.Entry(shoghlgrop).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!shoghlgropExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/shoghlgrops
        [ResponseType(typeof(shoghlgrop))]
        public IHttpActionResult Postshoghlgrop(shoghlgrop shoghlgrop)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.shoghlgrops.Add(shoghlgrop);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shoghlgrop.id }, shoghlgrop);
        }

        // DELETE: api/shoghlgrops/5
        [ResponseType(typeof(shoghlgrop))]
        public IHttpActionResult Deleteshoghlgrop(int id)
        {
            shoghlgrop shoghlgrop = db.shoghlgrops.Find(id);
            if (shoghlgrop == null)
            {
                return NotFound();
            }

            db.shoghlgrops.Remove(shoghlgrop);
            db.SaveChanges();

            return Ok(shoghlgrop);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool shoghlgropExists(int id)
        {
            return db.shoghlgrops.Count(e => e.id == id) > 0;
        }
    }
}