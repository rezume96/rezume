﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class shoghls1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: shoghls1
        public ActionResult Index()
        {
            return View(db.shoghls.ToList());
        }

        // GET: shoghls1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghl shoghl = db.shoghls.Find(id);
            if (shoghl == null)
            {
                return HttpNotFound();
            }
            return View(shoghl);
        }

        // GET: shoghls1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: shoghls1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name")] shoghl shoghl)
        {
            if (ModelState.IsValid)
            {
                db.shoghls.Add(shoghl);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(shoghl);
        }

        // GET: shoghls1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghl shoghl = db.shoghls.Find(id);
            if (shoghl == null)
            {
                return HttpNotFound();
            }
            return View(shoghl);
        }

        // POST: shoghls1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name")] shoghl shoghl)
        {
            if (ModelState.IsValid)
            {
                db.Entry(shoghl).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shoghl);
        }

        // GET: shoghls1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghl shoghl = db.shoghls.Find(id);
            if (shoghl == null)
            {
                return HttpNotFound();
            }
            return View(shoghl);
        }

        // POST: shoghls1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            shoghl shoghl = db.shoghls.Find(id);
            db.shoghls.Remove(shoghl);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
