﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class shoghlsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/shoghls
        public IQueryable<shoghl> Getshoghls()
        {
            return db.shoghls;
        }

        // GET: api/shoghls/5
        [ResponseType(typeof(shoghl))]
        public IHttpActionResult Getshoghl(int id)
        {
            shoghl shoghl = db.shoghls.Find(id);
            if (shoghl == null)
            {
                return NotFound();
            }

            return Ok(shoghl);
        }

        // PUT: api/shoghls/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putshoghl(int id, shoghl shoghl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shoghl.id)
            {
                return BadRequest();
            }

            db.Entry(shoghl).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!shoghlExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/shoghls
        [ResponseType(typeof(shoghl))]
        public IHttpActionResult Postshoghl(shoghl shoghl)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.shoghls.Add(shoghl);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shoghl.id }, shoghl);
        }

        // DELETE: api/shoghls/5
        [ResponseType(typeof(shoghl))]
        public IHttpActionResult Deleteshoghl(int id)
        {
            shoghl shoghl = db.shoghls.Find(id);
            if (shoghl == null)
            {
                return NotFound();
            }

            db.shoghls.Remove(shoghl);
            db.SaveChanges();

            return Ok(shoghl);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool shoghlExists(int id)
        {
            return db.shoghls.Count(e => e.id == id) > 0;
        }
    }
}