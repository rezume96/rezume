﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class shoghlusers1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: tahsils1
        public ActionResult Index(string userid, int rezkary)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (rezkary != 0)
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                var pishines = db.shoghlusers.Include(p => p.shoghls).Where(u => u.usersid == userid && u.rez == rezkary);
                return View(pishines.ToList());
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }


        // GET: shoghlusers1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghluser shoghluser = db.shoghlusers.Find(id);
            if (shoghluser == null)
            {
                return HttpNotFound();
            }
            return View(shoghluser);
        }

        // GET: tahsils1/Create
        public ActionResult Create(int rezkary, int flag)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (flag == 0)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary + 1;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 1)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 2)
                {

                    ViewBag.rezkary = rezkary + 1;
                    ViewBag.reztahsil = 0;
                    ViewBag.flag = flag;
                }
                ViewBag.shoghlid = new SelectList(db.shoghls, "id", "name");
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // POST: tahsils1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,shoghlid,rez")] shoghluser shoghluser)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.Where(g => g.Email == s).FirstOrDefault();
                    if (shoghluser.rez != 0 && shoghluser.rez > user.tedadrezkary)
                    {
                        user.tedadrezkary = shoghluser.rez;
                        pdf p = new pdf();
                        p.pdfname = "رزومه کاری" + shoghluser.rez.ToString();
                        p.rezkary = shoghluser.rez;
                        p.reztahsil = 0;
                        p.usersid = user.Id;
                        db.pdfs.Add(p);
                        shoghluser.usersid = user.Id;
                        db.shoghlusers.Add(shoghluser);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = shoghluser.rez, flag = 1 });
                    }
                    else if (shoghluser.rez != 0 && shoghluser.rez == user.tedadrezkary)
                    {
                        shoghluser.usersid = user.Id;
                        db.shoghlusers.Add(shoghluser);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = shoghluser.rez, flag = 1 });
                    }
                }
                ViewBag.shoghlid = new SelectList(db.zabans, "id", "name", shoghluser.shoghlid);
                return View(shoghluser);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        public ActionResult newshoghluser(int rezkary)
        {
            if (rezkary != 0)
            {
                ViewBag.rezkary = rezkary;
            }
            ViewBag.shoghlid = new SelectList(db.shoghls, "id", "name");
            return View();
        }
        [HttpPost]
        public ActionResult newshoghluser([Bind(Include = "id,usersid,shoghlid,rez")] shoghluser shoghluser)
        {

            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    db.Users.Where(g => g.Email == s).Load();
                    shoghluser.usersid = db.Users.Local[0].Id;
                    db.shoghlusers.Add(shoghluser);
                    db.SaveChanges();
                    return RedirectToAction("Index", null, new { controller = "shoghlusers1", action = "Index", userid = db.Users.Local[0].Id, rezkary = shoghluser.rez });
                }
                ViewBag.shoghlid = new SelectList(db.shoghls, "id", "name", shoghluser.shoghlid);
                return View(shoghluser);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        // GET: tahsils1/Edit/5
        public ActionResult Edit(int? id)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghluser shoghluser = db.shoghlusers.Find(id);
            if (shoghluser == null)
            {
                return HttpNotFound();
            }
            ViewBag.shoghlid = new SelectList(db.shoghls, "id", "name", shoghluser.shoghlid);
            return View(shoghluser);
        }

        // POST: shoghlusers1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,shoghlid,rez")] shoghluser shoghluser)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                db.Entry(shoghluser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", null, new { controller = "shoghlusers1", action = "Index", userid = db.Users.Local[0].Id, rezkary = shoghluser.rez });
            }
            ViewBag.shoghlid = new SelectList(db.shoghls, "id", "name", shoghluser.shoghlid);
            return View(shoghluser);
        }

        // GET: shoghlusers1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            shoghluser shoghluser = db.shoghlusers.Find(id);
            if (shoghluser == null)
            {
                return HttpNotFound();
            }
            return View(shoghluser);
        }

        // POST: shoghlusers1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            shoghluser shoghluser = db.shoghlusers.Find(id);
            db.shoghlusers.Remove(shoghluser);
            db.SaveChanges();
            return RedirectToAction("Index", null, new { controller = "shoghlusers1", action = "Index", userid = db.Users.Local[0].Id, rezkary = shoghluser.rez });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
