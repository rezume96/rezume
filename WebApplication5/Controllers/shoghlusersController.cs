﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class shoghlusersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/shoghlusers
        public IQueryable<shoghluser> Getshoghlusers()
        {
            return db.shoghlusers;
        }

        // GET: api/shoghlusers/5
        [ResponseType(typeof(shoghluser))]
        public IHttpActionResult Getshoghluser(int id)
        {
            shoghluser shoghluser = db.shoghlusers.Find(id);
            if (shoghluser == null)
            {
                return NotFound();
            }

            return Ok(shoghluser);
        }

        // PUT: api/shoghlusers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putshoghluser(int id, shoghluser shoghluser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shoghluser.id)
            {
                return BadRequest();
            }

            db.Entry(shoghluser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!shoghluserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/shoghlusers
        [ResponseType(typeof(shoghluser))]
        public IHttpActionResult Postshoghluser(shoghluser shoghluser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.shoghlusers.Add(shoghluser);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = shoghluser.id }, shoghluser);
        }

        // DELETE: api/shoghlusers/5
        [ResponseType(typeof(shoghluser))]
        public IHttpActionResult Deleteshoghluser(int id)
        {
            shoghluser shoghluser = db.shoghlusers.Find(id);
            if (shoghluser == null)
            {
                return NotFound();
            }

            db.shoghlusers.Remove(shoghluser);
            db.SaveChanges();

            return Ok(shoghluser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool shoghluserExists(int id)
        {
            return db.shoghlusers.Count(e => e.id == id) > 0;
        }
    }
}