﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class tahsilgetController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public class test
        {
            public int rez { get; set; }
            public string email { get; set; }
        }
        // GET: api/tahsilget
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/tahsilget/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/tahsilget
        public IHttpActionResult Post(test test)
        {
            try
            {

                db.Users.Where(g => g.Email == test.email).Load();
                if (db.Users.Local.Count == 0)
                {
                    string message = "BadRequest";
                    return Json(new { message });
                }
                string usersid = db.Users.Local[0].Id;
                db.tahsils.Where(g => g.usersid == usersid && g.reztahsil == test.rez).Load();
                db.sabeghes.Where(g => g.usersid == usersid && g.reztahsil == test.rez).Load();
                db.daneshes.Where(g => g.usersid == usersid && g.reztahsil == test.rez).Load();
                db.zabanusers.Where(g => g.usersid == usersid && g.reztahsil == test.rez).Load();
                db.eftekhars.Where(g => g.usersid == usersid && g.reztahsil == test.rez).Load();


                List<Tuple<usermodel, List<tahsilmodel>, List<sabeghemodel>, List<daneshmodel>, List<zabanusermodel>, List<eftekharmodel>>> kol =
                    new List<Tuple<usermodel, List<tahsilmodel>, List<sabeghemodel>, List<daneshmodel>, List<zabanusermodel>, List<eftekharmodel>>>();


                usermodel user = new usermodel();
                List<tahsilmodel> tahsils = new List<Models.tahsilmodel>();
                List<sabeghemodel> sabeghes = new List<sabeghemodel>();
                List<daneshmodel> daneshs = new List<daneshmodel>();
                List<zabanusermodel> zaban = new List<zabanusermodel>();
                List<eftekharmodel> eftekhars = new List<eftekharmodel>();

                foreach (var item in db.Users.Local)
                {
                    user.id = item.Id;
                    user.Name = item.Name;
                    user.pname = item.pname;
                    user.shenasname = item.shenasname;
                    user.D_tavalod = item.D_tavalod;
                    user.mojarad = item.mojarad;
                    user.jens = item.jens;
                    user.khedmat = item.khedmat;
                    user.Mobile = item.Mobile;
                    user.Address = item.Address;
                    user.shahr = item.shahr;
                    user.eshteghal = item.eshteghal;
                    user.pictur = item.pictur;
                    user.tozih = item.tozih;
                    user.tedadrezkary = item.tedadrezkary;
                    user.tedadreztahsil = item.tedadreztahsil;
                    user.email = item.Email;
                    user.phon = item.PhoneNumber;

                }
                modelfactory model = new modelfactory();
                foreach (var item in db.tahsils.Local)
                {
                    tahsils.Add(model.tahsil(item));
                }
                foreach (var item in db.sabeghes.Local)
                {
                    sabeghes.Add(model.sabeghe(item));
                }
                foreach (var item in db.daneshes.Local)
                {
                    daneshs.Add(model.danesh(item));
                }
                foreach (var item in db.zabanusers.Local)
                {
                    zaban.Add(model.zabanuser(item));
                }
                foreach (var item in db.eftekhars.Local)
                {
                    eftekhars.Add(model.eftekhar(item));
                }

                Tuple<usermodel, List<tahsilmodel>, List<sabeghemodel>, List<daneshmodel>, List<zabanusermodel>, List<eftekharmodel>> t1 =
                   new Tuple<usermodel, List<tahsilmodel>, List<sabeghemodel>, List<daneshmodel>, List<zabanusermodel>, List<eftekharmodel>>(user, tahsils, sabeghes, daneshs, zaban, eftekhars);
                kol.Add(t1);
                
                return Ok(kol);
            }
            catch (Exception)
            {
                string message = "InternalServerError";
                return Json(new { message });
            }
        }

        // PUT: api/tahsilget/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/tahsilget/5
        public void Delete(int id)
        {
        }
    }
}
