﻿using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class tahsilpostController : ApiController
    {
        // GET: api/tahsilpost
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/tahsilpost/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/tahsilpost
        public IHttpActionResult Post(List<Tuple<ApplicationUser, List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>>> kol)
        {
            foreach (var item in kol)
            {
                try
                {
                    userController ouser = new userController();
                    IHttpActionResult x = ouser.Get(item.Item1);
                    var j = x.GetType();
                    if (j.Name == "BadRequestResult")
                    {
                        string message = "BadRequestResult";
                        return Json(new { message });
                    }
                    else if (j.Name == "InternalServerErrorResult")
                    {

                        string message = "InternalServerError";
                        return Json(new { message });
                    }
                }
                catch (Exception)
                {
                    string message = "InternalServerError";
                    return Json(new { message });
                }
                try
                {

                    tahsilsController tahsil = new tahsilsController();
                    foreach (var item1 in item.Item2)
                    {
                        tahsil.Posttahsil(item1);
                    }

                    sabeghesController sabeghe = new sabeghesController();
                    foreach (var item1 in item.Item3)
                    {
                        sabeghe.Postsabeghe(item1);
                    }

                    daneshesController danesh = new daneshesController();
                    foreach (var item6 in item.Item4)
                    {
                        danesh.Postdanesh(item6);
                    }

                    zabanusersController zaban = new zabanusersController();
                    foreach (var item2 in item.Item5)
                    {
                        zaban.Postzabanuser(item2);
                    }


                    eftekharsController eft = new eftekharsController();
                    foreach (var item8 in item.Item6)
                    {
                        eft.Posteftekhar(item8);
                    }

                    pdfsController opdf = new pdfsController();
                    pdf op = new Models.pdf();
                    op.pdfname = "رزومه تحصیلی" + item.Item1.tedadreztahsil;
                    op.reztahsil = item.Item1.tedadreztahsil;
                    op.usersid = item.Item1.Id;
                    op.rezkary = 0;
                    opdf.Postpdf(op);
                    creatpdftahsilController f = new creatpdftahsilController();

                    var message = f.Post(item.Item1.Id,item.Item1.tedadreztahsil);
                    /////////////////////////////////////////////////////////////////////////////////////////////////////
                    
                    return Json(new { message });
                }
                catch (Exception)
                {
                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "InternalServerError";
                    return Json(new { message });
                }
            }
            return Ok();
        }

        // PUT: api/tahsilpost/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/tahsilpost/5
        public void Delete(int id)
        {
        }
    }
}
