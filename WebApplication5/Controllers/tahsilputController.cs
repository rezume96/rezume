﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class tahsilputController : ApiController
    {
        // GET: api/tahsilput
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/tahsilput/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/tahsilput
        public IHttpActionResult Puttahsil(List<Tuple<ApplicationUser, List<tahsil>, List<sabeghe>, List<danesh>, List<zabanuser>, List<eftekhar>>> kol)
        {
            foreach (var item in kol)
            {
                try
                {
                    userController ouser = new userController();
                    IHttpActionResult x = ouser.Get(item.Item1);
                    var j = x.GetType();
                    if (j.Name == "BadRequestResult")
                    {
                        string message = "BadRequestResult";
                        return Json(new { message });
                    }
                    else if (j.Name == "InternalServerErrorResult")
                    {

                        string message = "InternalServerError";
                        return Json(new { message });
                    }
                }
                catch (Exception)
                {
                    string message = "InternalServerError";
                    return Json(new { message });
                }

                ///////ذخیره دیگر اطلاعات
                try
                {


                    tahsilsController tah = new tahsilsController();
                    foreach (var item8 in item.Item2)
                    {
                        tah.Puttahsil(item8.id, item8);
                    }


                    sabeghesController sab = new sabeghesController();
                    foreach (var item8 in item.Item3)
                    {
                        sab.Putsabeghe(item8.id, item8);
                    }


                    daneshesController danesh = new daneshesController();
                    foreach (var item8 in item.Item4)
                    {
                        danesh.Putdanesh(item8.id, item8);
                    }
                    zabanusersController zab = new zabanusersController();
                    foreach (var item8 in item.Item5)
                    {
                        zab.Putzabanuser(item8.id, item8);
                    }
                    eftekharsController eft = new eftekharsController();
                    foreach (var item8 in item.Item6)
                    {
                        eft.Puteftekhar(item8.id, item8);
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "ok";
                    return Json(new { message });
                }
                catch (Exception)
                {
                    /////////////////////////////////////////////////////////////////////////////////////////////////////

                    string message = "InternalServerError";
                    return Json(new { message });
                }
            }
            return Ok();
        }

        // PUT: api/tahsilput/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/tahsilput/5
        public void Delete(int id)
        {
        }
    }
}
