﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class tahsils1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: tahsils1
        public ActionResult Index(string userid, int rezkary, int reztahsil)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (rezkary != 0)
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = reztahsil;
                return View(db.tahsils.Where(u => u.usersid == userid && u.rezkary == rezkary).ToList());
            }
            else
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = reztahsil;
                return View(db.tahsils.Where(u => u.usersid == userid && u.reztahsil == reztahsil).ToList());
            }
        }

        // GET: tahsils1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tahsil tahsil = db.tahsils.Find(id);
            if (tahsil == null)
            {
                return HttpNotFound();
            }
            return View(tahsil);
        }

        // GET: tahsils1/Create
        public ActionResult Create(int? rezkary, int? reztahsil, int? flag)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (rezkary == null || reztahsil == null || flag == null)
                {
                    return RedirectToAction("Login", new { controller = "register", action = "Login" });
                }
                if (flag == 0)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary + 1;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                    else
                    {
                        ViewBag.rezkary = 0;
                        ViewBag.reztahsil = reztahsil + 1;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 1)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                    else
                    {
                        ViewBag.rezkary = 0;
                        ViewBag.reztahsil = reztahsil;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 2)
                {

                    ViewBag.rezkary = rezkary + 1;
                    ViewBag.reztahsil = 0;
                    ViewBag.flag = flag;
                }
                else if (flag == 3)
                {

                    ViewBag.rezkary = 0;
                    ViewBag.reztahsil = reztahsil + 1;
                    ViewBag.flag = flag;
                }
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // POST: tahsils1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,reshteh,gerayesh,uni,maghta,moadel,shoro,payan,tozih,rezkary,reztahsil")] tahsil tahsil)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.Where(g => g.Email == s).FirstOrDefault();
                    if (tahsil.rezkary != 0 && tahsil.rezkary > user.tedadrezkary)
                    {
                        user.tedadrezkary = tahsil.rezkary;
                        pdf p = new pdf();
                        p.pdfname = "رزومه کاری" + tahsil.rezkary.ToString();
                        p.rezkary = tahsil.rezkary;
                        p.reztahsil = 0;
                        p.usersid = user.Id;
                        db.pdfs.Add(p);
                        tahsil.usersid = user.Id;
                        db.tahsils.Add(tahsil);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = tahsil.rezkary, flag = 1 });
                    }
                    else if (tahsil.rezkary != 0 && tahsil.rezkary == user.tedadrezkary)
                    {
                        tahsil.usersid = user.Id;
                        db.tahsils.Add(tahsil);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = tahsil.rezkary, flag = 1 });
                    }
                    else if (tahsil.reztahsil != 0 && tahsil.reztahsil > user.tedadreztahsil)
                    {
                        user.tedadreztahsil = tahsil.reztahsil;
                        pdf p = new pdf();
                        p.pdfname = "رزومه تحصیلی" + tahsil.reztahsil.ToString();
                        p.reztahsil = tahsil.reztahsil;
                        p.usersid = user.Id;
                        p.rezkary = 0;
                        db.pdfs.Add(p);
                        tahsil.usersid = user.Id;
                        db.tahsils.Add(tahsil);
                        db.SaveChanges();
                        return RedirectToAction("newtahsil", new { controller = "pdfs1", action = "newtahsil", rez = tahsil.reztahsil, flag = 1 });
                    }
                    else if (tahsil.reztahsil != 0 && tahsil.reztahsil == user.tedadreztahsil)
                    {
                        tahsil.usersid = user.Id;
                        db.tahsils.Add(tahsil);
                        db.SaveChanges();
                        return RedirectToAction("newtahsil", new { controller = "pdfs1", action = "newtahsil", rez = tahsil.reztahsil, flag = 1 });
                    }
                }
                return View(tahsil);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        public ActionResult newtahsil(int rezkary,int reztahsil)
        {
            if (rezkary != 0)
            {
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = 0;
            }
            else
            {
                ViewBag.rezkary = 0;
                ViewBag.reztahsil = reztahsil;
            }
            return View();
        }
        [HttpPost]
        public ActionResult newtahsil([Bind(Include = "id,usersid,reshteh,gerayesh,uni,maghta,moadel,shoro,payan,tozih,rezkary,reztahsil")] tahsil tahsil)
        {

            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    db.Users.Where(g => g.Email == s).Load();
                    tahsil.usersid = db.Users.Local[0].Id;
                    db.tahsils.Add(tahsil);
                    db.SaveChanges();
                    return RedirectToAction("Index",null,new { controller="tahsils1",action="Index",userid=db.Users.Local[0].Id,rezkary=tahsil.rezkary,reztahsil=tahsil.reztahsil});
                }
                return View(tahsil);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        // GET: tahsils1/Edit/5
        public ActionResult Edit(int? id)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tahsil tahsil = db.tahsils.Find(id);
            if (tahsil == null)
            {
                return HttpNotFound();
            }
            return View(tahsil);
        }

        // POST: tahsils1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,reshteh,gerayesh,uni,maghta,moadel,shoro,payan,tozih,rezkary,reztahsil")] tahsil tahsil)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                db.Entry(tahsil).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index",null,new { userid=tahsil.usersid,rezkary=tahsil.rezkary,reztahsil=tahsil.reztahsil});
            }
            return View(tahsil);
        }

        // GET: tahsils1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tahsil tahsil = db.tahsils.Find(id);
            if (tahsil == null)
            {
                return HttpNotFound();
            }
            return View(tahsil);
        }

        // POST: tahsils1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tahsil tahsil = db.tahsils.Find(id);
            db.tahsils.Remove(tahsil);
            db.SaveChanges();
            return RedirectToAction("Index", null, new { userid = tahsil.usersid, rezkary = tahsil.rezkary, reztahsil = tahsil.reztahsil });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
