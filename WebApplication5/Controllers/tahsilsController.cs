﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class tahsilsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/tahsils
        public IQueryable<tahsil> Gettahsils()
        {
            return db.tahsils;
        }

        // GET: api/tahsils/5
        [ResponseType(typeof(tahsil))]
        public IHttpActionResult Gettahsil(int id)
        {
            tahsil tahsil = db.tahsils.Find(id);
            if (tahsil == null)
            {
                return NotFound();
            }

            return Ok(tahsil);
        }

        // PUT: api/tahsils/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttahsil(int id, tahsil tahsil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tahsil.id)
            {
                return BadRequest();
            }

            db.Entry(tahsil).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tahsilExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/tahsils
        [ResponseType(typeof(tahsil))]
        public IHttpActionResult Posttahsil(tahsil tahsil)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tahsils.Add(tahsil);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tahsil.id }, tahsil);
        }

        // DELETE: api/tahsils/5
        [ResponseType(typeof(tahsil))]
        public IHttpActionResult Deletetahsil(int id)
        {
            tahsil tahsil = db.tahsils.Find(id);
            if (tahsil == null)
            {
                return NotFound();
            }

            db.tahsils.Remove(tahsil);
            db.SaveChanges();

            return Ok(tahsil);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tahsilExists(int id)
        {
            return db.tahsils.Count(e => e.id == id) > 0;
        }
    }
}