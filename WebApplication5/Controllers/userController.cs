﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication5.Models;
namespace WebApplication5.Controllers
{
    public class userController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public IHttpActionResult Get(ApplicationUser user1)
        {
            try
            {
                db.Users.Where(g => g.Id == user1.Id).Load();
                if (db.Users.Local.Count != 0)
                {
                    if (user1.shenasname == "" || user1.shenasname == null || user1.shenasname == string.Empty)
                    {
                        ApplicationDbContext db1 = new ApplicationDbContext();
                        var temp = db1.Users.Where(f => f.Id == user1.Id).FirstOrDefault();
                        temp.Name = user1.Name;
                        temp.pname = user1.pname;
                        temp.shenasname = user1.shenasname;
                        temp.D_tavalod = user1.D_tavalod;
                        temp.mojarad = user1.mojarad;
                        temp.jens = user1.jens;
                        temp.khedmat = user1.khedmat;
                        temp.Mobile = user1.Mobile;
                        temp.Address = user1.Address;
                        temp.shahr = user1.shahr;
                        temp.eshteghal = user1.eshteghal;
                        temp.pictur = user1.pictur;
                        temp.tozih = user1.tozih;
                        temp.tedadrezkary = user1.tedadrezkary;
                        temp.tedadreztahsil = user1.tedadreztahsil;
                        temp.Email = user1.Email;
                        temp.PhoneNumber = user1.PhoneNumber;
                        try
                        {
                            db1.SaveChanges();
                            db1.Dispose();
                            db1 = null;
                            return Ok();
                        }
                        catch (Exception)
                        {
                            return InternalServerError();
                        }
                    }
                    //چک کردن کد ملی
                    char[] chArray = user1.shenasname.ToCharArray();
                    int[] numArray = new int[chArray.Length];
                    for (int i = 0; i < chArray.Length; i++)
                    {
                        numArray[i] = (int)char.GetNumericValue(chArray[i]);
                    }
                    int num2 = numArray[9];
                    switch (user1.shenasname)
                    {
                        case "0000000000":
                        case "1111111111":
                        case "22222222222":
                        case "33333333333":
                        case "4444444444":
                        case "5555555555":
                        case "6666666666":
                        case "7777777777":
                        case "8888888888":
                        case "9999999999":
                            return BadRequest();
                    }
                    int num3 = ((((((((numArray[0] * 10) + (numArray[1] * 9)) + (numArray[2] * 8)) + (numArray[3] * 7)) + (numArray[4] * 6)) + (numArray[5] * 5)) + (numArray[6] * 4)) + (numArray[7] * 3)) + (numArray[8] * 2);
                    int num4 = num3 - ((num3 / 11) * 11);
                    ///اگر کد ملی درست بود
                    if ((((num4 == 0) && (num2 == num4)) || ((num4 == 1) && (num2 == 1))) || ((num4 > 1) && (num2 == Math.Abs((int)(num4 - 11)))))
                    {
                        db.Users.Where(c => c.shenasname == user1.shenasname).Load();
                        string id = "";
                        if (db.Users.Local.Count != 0)
                        {
                            id = db.Users.Local[0].Id;
                        }
                        //چک کردن وجود کد ملی
                        if (db.Users.Local.Count == 0 || id == user1.Id)
                        {
                            ApplicationDbContext db1 = new ApplicationDbContext();
                            var temp = db1.Users.Where(f => f.Id == user1.Id).FirstOrDefault();
                            temp.Name = user1.Name;
                            temp.pname = user1.pname;
                            temp.shenasname = user1.shenasname;
                            temp.D_tavalod = user1.D_tavalod;
                            temp.mojarad = user1.mojarad;
                            temp.jens = user1.jens;
                            temp.khedmat = user1.khedmat;
                            temp.Mobile = user1.Mobile;
                            temp.Address = user1.Address;
                            temp.shahr = user1.shahr;
                            temp.eshteghal = user1.eshteghal;
                            temp.pictur = user1.pictur;
                            temp.tozih = user1.tozih;
                            temp.tedadrezkary = user1.tedadrezkary;
                            temp.tedadreztahsil = user1.tedadreztahsil;
                            temp.Email = user1.Email;
                            temp.PhoneNumber = user1.PhoneNumber;
                            try
                            {
                                db1.SaveChanges();
                                db1.Dispose();
                                db1 = null;
                                return Ok();
                            }
                            catch (Exception)
                            {
                                return InternalServerError();
                            }
                        }
                        else
                        {
                            return BadRequest();
                        }
                    }
                    else
                    {
                        return BadRequest();
                    }

                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
    }
}