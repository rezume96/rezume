﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class zabans1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: zabans1
        public ActionResult Index()
        {
            return View(db.zabans.ToList());
        }

        // GET: zabans1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zaban zaban = db.zabans.Find(id);
            if (zaban == null)
            {
                return HttpNotFound();
            }
            return View(zaban);
        }

        // GET: zabans1/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: zabans1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name")] zaban zaban)
        {
            if (ModelState.IsValid)
            {
                db.zabans.Add(zaban);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(zaban);
        }

        // GET: zabans1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zaban zaban = db.zabans.Find(id);
            if (zaban == null)
            {
                return HttpNotFound();
            }
            return View(zaban);
        }

        // POST: zabans1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name")] zaban zaban)
        {
            if (ModelState.IsValid)
            {
                db.Entry(zaban).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(zaban);
        }

        // GET: zabans1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zaban zaban = db.zabans.Find(id);
            if (zaban == null)
            {
                return HttpNotFound();
            }
            return View(zaban);
        }

        // POST: zabans1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            zaban zaban = db.zabans.Find(id);
            db.zabans.Remove(zaban);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
