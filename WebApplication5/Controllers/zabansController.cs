﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class zabansController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/zabans
        public IQueryable<zaban> Getzabans()
        {
            return db.zabans;
        }

        // GET: api/zabans/5
        [ResponseType(typeof(zaban))]
        public IHttpActionResult Getzaban(int id)
        {
            zaban zaban = db.zabans.Find(id);
            if (zaban == null)
            {
                return NotFound();
            }

            return Ok(zaban);
        }

        // PUT: api/zabans/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putzaban(int id, zaban zaban)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zaban.id)
            {
                return BadRequest();
            }

            db.Entry(zaban).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!zabanExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/zabans
        [ResponseType(typeof(zaban))]
        public IHttpActionResult Postzaban(zaban zaban)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.zabans.Add(zaban);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = zaban.id }, zaban);
        }

        // DELETE: api/zabans/5
        [ResponseType(typeof(zaban))]
        public IHttpActionResult Deletezaban(int id)
        {
            zaban zaban = db.zabans.Find(id);
            if (zaban == null)
            {
                return NotFound();
            }

            db.zabans.Remove(zaban);
            db.SaveChanges();

            return Ok(zaban);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool zabanExists(int id)
        {
            return db.zabans.Count(e => e.id == id) > 0;
        }
    }
}