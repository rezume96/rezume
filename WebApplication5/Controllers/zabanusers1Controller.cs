﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class zabanusers1Controller : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        

        // GET: daneshes1
        public ActionResult Index(string userid, int rezkary, int reztahsil)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (rezkary != 0)
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = reztahsil;
                var zabanusers = db.zabanusers.Include(z => z.zabans).Where(u => u.usersid == userid && u.rezkary == rezkary);
                return View(zabanusers.ToList());
            }
            else
            {
                ViewBag.userid = userid;
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = reztahsil;
                var zabanusers = db.zabanusers.Include(z => z.zabans).Where(u => u.usersid == userid && u.rezkary == rezkary);
                return View(zabanusers.ToList());
            }
        }
        // GET: zabanusers1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zabanuser zabanuser = db.zabanusers.Find(id);
            if (zabanuser == null)
            {
                return HttpNotFound();
            }
            return View(zabanuser);
        }

        // GET: tahsils1/Create
        public ActionResult Create(int? rezkary, int? reztahsil, int? flag)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (flag == 0)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary + 1;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                    else
                    {
                        ViewBag.rezkary = 0;
                        ViewBag.reztahsil = reztahsil + 1;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 1)
                {
                    if (rezkary != 0)
                    {
                        ViewBag.rezkary = rezkary;
                        ViewBag.reztahsil = 0;
                        ViewBag.flag = flag;
                    }
                    else
                    {
                        ViewBag.rezkary = 0;
                        ViewBag.reztahsil = reztahsil;
                        ViewBag.flag = flag;
                    }
                }
                else if (flag == 2)
                {

                    ViewBag.rezkary = rezkary + 1;
                    ViewBag.reztahsil = 0;
                    ViewBag.flag = flag;
                }
                else if (flag == 3)
                {

                    ViewBag.rezkary = 0;
                    ViewBag.reztahsil = reztahsil + 1;
                    ViewBag.flag = flag;
                }
                ViewBag.zabanid = new SelectList(db.zabans, "id", "name");
                return View();
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        // POST: tahsils1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,usersid,mizan,madrak,zabanid,rezkary,reztahsil")] zabanuser zabanuser)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    var user = db.Users.Where(g => g.Email == s).FirstOrDefault();
                    if (zabanuser.rezkary != 0 && zabanuser.rezkary > user.tedadrezkary)
                    {
                        user.tedadrezkary = zabanuser.rezkary;
                        pdf p = new pdf();
                        p.pdfname = "رزومه کاری" + zabanuser.rezkary.ToString();
                        p.rezkary = zabanuser.rezkary;
                        p.reztahsil = 0;
                        p.usersid = user.Id;
                        db.pdfs.Add(p);
                        zabanuser.usersid = user.Id;
                        db.zabanusers.Add(zabanuser);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = zabanuser.rezkary, flag = 1 });
                    }
                    else if (zabanuser.rezkary != 0 && zabanuser.rezkary == user.tedadrezkary)
                    {
                        zabanuser.usersid = user.Id;
                        db.zabanusers.Add(zabanuser);
                        db.SaveChanges();
                        return RedirectToAction("newkary", new { controller = "pdfs1", action = "newkary", rez = zabanuser.rezkary, flag = 1 });
                    }
                    else if (zabanuser.reztahsil != 0 && zabanuser.reztahsil > user.tedadreztahsil)
                    {
                        user.tedadreztahsil = zabanuser.reztahsil;
                        pdf p = new pdf();
                        p.pdfname = "رزومه تحصیلی" + zabanuser.reztahsil.ToString();
                        p.reztahsil = zabanuser.reztahsil;
                        p.usersid = user.Id;
                        p.rezkary = 0;
                        db.pdfs.Add(p);
                        zabanuser.usersid = user.Id;
                        db.zabanusers.Add(zabanuser);
                        db.SaveChanges();
                        return RedirectToAction("newtahsil", new { controller = "pdfs1", action = "newtahsil", rez = zabanuser.reztahsil, flag = 1 });
                    }
                    else if (zabanuser.reztahsil != 0 && zabanuser.reztahsil == user.tedadreztahsil)
                    {
                        zabanuser.usersid = user.Id;
                        db.zabanusers.Add(zabanuser);
                        db.SaveChanges();
                        return RedirectToAction("newtahsil", new { controller = "pdfs1", action = "newtahsil", rez = zabanuser.reztahsil, flag = 1 });
                    }
                }
                ViewBag.zabanid = new SelectList(db.zabans, "id", "name", zabanuser.zabanid);
                return View(zabanuser);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }

        public ActionResult newzabanuser(int rezkary, int reztahsil)
        {
            if (rezkary != 0)
            {
                ViewBag.rezkary = rezkary;
                ViewBag.reztahsil = 0;
            }
            else
            {
                ViewBag.rezkary = 0;
                ViewBag.reztahsil = reztahsil;
            }
            ViewBag.zabanid = new SelectList(db.zabans, "id", "name");
            return View();
        }
        [HttpPost]
        public ActionResult newzabanuser([Bind(Include = "id,usersid,mizan,madrak,zabanid,rezkary,reztahsil")] zabanuser zabanuser)
        {

            checkController v = new checkController();
            string s = v.getcheck();
            if (s != "")
            {
                if (ModelState.IsValid)
                {
                    db.Users.Where(g => g.Email == s).Load();
                    zabanuser.usersid = db.Users.Local[0].Id;
                    db.zabanusers.Add(zabanuser);
                    db.SaveChanges();
                    return RedirectToAction("Index", null, new { controller = "zabanusers1", action = "Index", userid = db.Users.Local[0].Id, rezkary = zabanuser.rezkary, reztahsil = zabanuser.reztahsil });
                }
                ViewBag.zabanid = new SelectList(db.zabans, "id", "name", zabanuser.zabanid);
                return View(zabanuser);
            }
            return RedirectToAction("Login", new { controller = "register", action = "Login" });
        }
        // GET: tahsils1/Edit/5
        public ActionResult Edit(int? id)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zabanuser zabanuser = db.zabanusers.Find(id);
            if (zabanuser == null)
            {
                return HttpNotFound();
            }
            ViewBag.zabanid = new SelectList(db.zabans, "id", "name", zabanuser.zabanid);
            return View(zabanuser);
        }

        // POST: zabanusers1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,usersid,mizan,madrak,zabanid,rezkary,reztahsil")] zabanuser zabanuser)
        {
            checkController v = new checkController();
            string s = v.getcheck();
            if (s == "")
            {
                return RedirectToAction("Login", new { controller = "register", action = "Login" });
            }
            if (ModelState.IsValid)
            {
                db.Entry(zabanuser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", null, new { controller = "zabanusers1", action = "Index", userid = db.Users.Local[0].Id, rezkary = zabanuser.rezkary, reztahsil = zabanuser.reztahsil });
            }
            ViewBag.zabanid = new SelectList(db.zabans, "id", "name", zabanuser.zabanid);
            return View(zabanuser);
        }

        // GET: zabanusers1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            zabanuser zabanuser = db.zabanusers.Find(id);
            if (zabanuser == null)
            {
                return HttpNotFound();
            }
            return View(zabanuser);
        }

        // POST: zabanusers1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            zabanuser zabanuser = db.zabanusers.Find(id);
            db.zabanusers.Remove(zabanuser);
            db.SaveChanges();
            return RedirectToAction("Index", null, new { controller = "zabanusers1", action = "Index", userid = db.Users.Local[0].Id, rezkary = zabanuser.rezkary, reztahsil = zabanuser.reztahsil });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
