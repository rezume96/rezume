﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class zabanusersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/zabanusers
        public IQueryable<zabanuser> Getzabanusers()
        {
            return db.zabanusers;
        }

        // GET: api/zabanusers/5
        [ResponseType(typeof(zabanuser))]
        public IHttpActionResult Getzabanuser(int id)
        {
            zabanuser zabanuser = db.zabanusers.Find(id);
            if (zabanuser == null)
            {
                return NotFound();
            }

            return Ok(zabanuser);
        }

        // PUT: api/zabanusers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putzabanuser(int id, zabanuser zabanuser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zabanuser.id)
            {
                return BadRequest();
            }

            db.Entry(zabanuser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!zabanuserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/zabanusers
        [ResponseType(typeof(zabanuser))]
        public IHttpActionResult Postzabanuser(zabanuser zabanuser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.zabanusers.Add(zabanuser);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = zabanuser.id }, zabanuser);
        }

        // DELETE: api/zabanusers/5
        [ResponseType(typeof(zabanuser))]
        public IHttpActionResult Deletezabanuser(int id)
        {
            zabanuser zabanuser = db.zabanusers.Find(id);
            if (zabanuser == null)
            {
                return NotFound();
            }

            db.zabanusers.Remove(zabanuser);
            db.SaveChanges();

            return Ok(zabanuser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool zabanuserExists(int id)
        {
            return db.zabanusers.Count(e => e.id == id) > 0;
        }
    }
}