﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication5.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public partial class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public partial class ApplicationUser
    {
        [MaxLength(50)]
        [DisplayName("نام و نام خانوادگی")]
        public string Name { get; set; }
        [DisplayName("نام پدر")]
        [MaxLength(50)]
        public string pname { get; set; }
        [DisplayName("شماره ملی")]
        [MaxLength(10)]
        public string shenasname { get; set; }

        [DisplayName("تاریخ تولد")]
        [Column(TypeName = "datetime2")]
        public System.DateTime? D_tavalod { get; set; }
        [MaxLength(10)]
        [DisplayName("وضعیت تاهل")]
        public string mojarad { get; set; }
        [MaxLength(10)]
        [DisplayName("جنسیت")]
        public string jens { get; set; }
        [MaxLength(20)]
        [DisplayName("وضعیت خدمت سربازی")]
        public string khedmat { get; set; }
        [MaxLength(30)]
        [DisplayName("نوع معافیت")]
        public string nomoaf { get; set; }
        [DisplayName("تاریخ پایان خدمت")]
        [Column(TypeName = "datetime2")]
        public System.DateTime? payankhedmat { get; set; }
        [MaxLength(11)]
        [DisplayName("شماره موبایل")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("09(1[0-9]|3[1-9]|2[1-9])?[0-9]{3}?[0-9]{4}", ErrorMessage = "فرمت موبایل خود را صحیح وارد نمایید")]
        public string Mobile { get; set; }
        [DisplayName("آدرس")]
        public string Address { get; set; }
        [DisplayName("شهر محل سکونت")]
        public string shahr { get; set; }
        [DisplayName("وضعیت اشتغال")]
        public string eshteghal { get; set; }
        [DisplayName("عکس")]
        public string pictur { get; set; }
        [DisplayName("توضیحات")]
        public string tozih { get; set; }
        [DisplayName("علاقه")]
        public string alaghe { get; set; }
        [DisplayName("محل کار")]
        public string mahalkar { get; set; }
        [DisplayName("حقوق")]
        public string hoghogh { get; set; }
        [DisplayName("آدرس پروفایل")]
        public string profil { get; set; }
        public int tedadrezkary { get; set; }
        public int tedadreztahsil { get; set; }
        public virtual IList<danesh> daneshs { get; set; }
        public virtual IList<eftekhar> eftekhars { get; set; }
        public virtual IList<narm> narms { get; set; }
        public virtual IList<pdf> pdfs { get; set; }
        public virtual IList<pishine> pishines { get; set; }
        public virtual IList<project> projects { get; set; }
        public virtual IList<sabeghe> sabeghes { get; set; }
        public virtual IList<shoghluser> shoghlusers { get; set; }
        public virtual IList<tahsil> tahsils { get; set; }
        public virtual IList<zabanuser> zabanusers { get; set; }

    }
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<WebApplication5.Models.danesh> daneshes { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.eftekhar> eftekhars { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.narm> narms { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.pdf> pdfs { get; set; }
        public System.Data.Entity.DbSet<WebApplication5.Models.project> projects { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.sabeghe> sabeghes { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.shoghl> shoghls { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.shoghluser> shoghlusers { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.tahsil> tahsils { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.zaban> zabans { get; set; }

        public System.Data.Entity.DbSet<WebApplication5.Models.zabanuser> zabanusers { get; set; }
        public System.Data.Entity.DbSet<WebApplication5.Models.pishine> pishines { get; set; }
        public System.Data.Entity.DbSet<WebApplication5.Models.shoghlgrop> shoghlgrops { get; set; }
    }
}