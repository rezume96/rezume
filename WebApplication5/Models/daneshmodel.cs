﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class daneshmodel
    {
        public daneshmodel()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("حوزه")
        , MaxLength(50, ErrorMessage = "حداکثر طول حوزه 50 کاراکتر است")]
        public string hoze { get; set; }
        [DisplayName("تسلط")
        , MaxLength(20, ErrorMessage = "حداکثر طول تسلط 20 کاراکتر است")]
        public string mizan { get; set; }
        [DisplayName("مدرک")
        , MaxLength(50, ErrorMessage = "حداکثر طول مدرک 50 کاراکتر است")]
        public string madrak { get; set; }
        public int rezkary { get; set; }
        public int reztahsil { get; set; }

        [MaxLength(128)]
        public string usersid { get; set; }
    }
}