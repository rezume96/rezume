﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class eftekhar
    {
        public eftekhar()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("سال")
        , MaxLength(4, ErrorMessage = "حداکثر طول نام 20 کاراکتر است")]
        public string sal { get; set; }
        [DisplayName("توضیح")]
        public string tozih { get; set; }
        public int rezkary { get; set; }
        public int reztahsil { get; set; }

        [MaxLength(128)]
        public virtual string usersid { get; set; }
        public virtual ApplicationUser users { get; set; }
    }
}