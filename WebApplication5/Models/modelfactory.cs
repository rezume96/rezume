﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class modelfactory
    {
        public daneshmodel danesh(danesh danesh)
        {
            return new daneshmodel
            {
                id = danesh.id,
                hoze = danesh.hoze,
                madrak = danesh.madrak,
                mizan = danesh.mizan,
                rezkary = danesh.rezkary,
                reztahsil = danesh.reztahsil,
                usersid = danesh.usersid
            };
        }
        public eftekharmodel eftekhar(eftekhar ef)
        {
            return new eftekharmodel
            {
                id = ef.id,
                rezkary = ef.rezkary,
                reztahsil = ef.reztahsil,
                sal = ef.sal,
                tozih = ef.tozih,
                usersid = ef.usersid
            };
        }
        public narmmodel narm(narm narm)
        {
            return new narmmodel
            {
                id = narm.id,
                rez = narm.rez,
                madrak = narm.madrak,
                mizan = narm.mizan,
                title = narm.title,
                usersid = narm.usersid
            };
        }
        public pishinemodel pishine(pishine pis)
        {
            return new pishinemodel
            {
                id = pis.id,
                usersid = pis.usersid,
                sath = pis.sath,
                sazmanname = pis.sazmanname,
                shoghlgropid = pis.shoghlgropid,
                payan = pis.payan,
                rez = pis.rez,
                shoro = pis.shoro,
                title = pis.title,
                tozih = pis.tozih
            };
        }
        public projectmodel project(project pro)
        {
            return new projectmodel
            {
                id = pro.id,
                rez = pro.rez,
                sal = pro.sal,
                semat = pro.semat,
                title = pro.title,
                tozih = pro.tozih,
                usersid = pro.usersid
            };
        }
        public sabeghemodel sabeghe(sabeghe sa)
        {
            return new sabeghemodel
            {
                id = sa.id,
                rezkary = sa.rezkary,
                reztahsil = sa.reztahsil,
                sal = sa.sal,
                tozih = sa.tozih,
                usersid = sa.usersid
            };
        }
        public shoghlusermodel shoghluser(shoghluser sh)
        {
            return new shoghlusermodel
            {
                id = sh.id,
                rez = sh.rez,
                shoghlid = sh.shoghlid,
                usersid = sh.usersid
            };
        }
        public tahsilmodel tahsil(tahsil tahsil)
        {
            return new tahsilmodel
            {
                id = tahsil.id,
                maghta = tahsil.maghta,
                moadel = tahsil.moadel,
                usersid = tahsil.usersid,
                uni = tahsil.uni,
                tozih = tahsil.tozih,
                gerayesh = tahsil.gerayesh,
                shoro = tahsil.shoro,
                payan = tahsil.payan,
                reshteh = tahsil.reshteh,
                rezkary = tahsil.rezkary,
                reztahsil = tahsil.reztahsil
            };
        }
        public zabanusermodel zabanuser(zabanuser zaban)
        {
            return new Models.zabanusermodel
            {
                id = zaban.id,
                madrak = zaban.madrak,
                mizan = zaban.mizan,
                reztahsil = zaban.reztahsil,
                rezkary = zaban.rezkary,
                usersid = zaban.usersid,
                zabanid = zaban.zabanid
            };
        }
        public usermodel user(ApplicationUser user)
        {
            return new usermodel
            {
                id = user.Id,
                email = user.Email,
                D_tavalod = user.D_tavalod,
                shahr = user.shahr,
                shenasname = user.shenasname,
                pictur = user.pictur,
                phon = user.PhoneNumber,
                mojarad = user.mojarad,
                Mobile = user.Mobile,
                Name = user.Name,
                Address = user.Address,
                alaghe = user.alaghe,
                jens = user.jens,
                khedmat = user.khedmat,
                tedadrezkary = user.tedadrezkary,
                tedadreztahsil = user.tedadreztahsil,
                eshteghal = user.eshteghal,
                hoghogh = user.hoghogh,
                mahalkar = user.mahalkar,
                pname = user.pname,
                tozih = user.tozih
            };
        }
    }
}