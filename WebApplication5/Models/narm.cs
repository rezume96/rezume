﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class narm
    {
        public narm()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("عنوان")
        , MaxLength(20, ErrorMessage = "حداکثر طول عنوان 20 کاراکتر است")]
        public string title { get; set; }
        [DisplayName("تسلط")
        , MaxLength(20, ErrorMessage = "حداکثر طول میزان تسلط 20 کاراکتر است")]
        public string mizan { get; set; }
        [DisplayName("مدرک")
        , MaxLength(50, ErrorMessage = "حداکثر طول نام مدرک 50 کاراکتر است")]
        public string madrak { get; set; }
        public int rez { get; set; }

        [MaxLength(128)]
        public virtual string usersid { get; set; }
        public virtual ApplicationUser users { get; set; }

    }
}