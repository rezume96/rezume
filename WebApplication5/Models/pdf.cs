﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class pdf
    {
        public pdf()
        {

        }
        [Key]
        public int id { get; set; }
        public string pdfname { get; set; }
        public int rezkary { get; set; }
        public int reztahsil { get; set; }

        [MaxLength(128)]
        public virtual string usersid { get; set; }
        public virtual ApplicationUser users { get; set; }
    }
}