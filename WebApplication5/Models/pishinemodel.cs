﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class pishinemodel
    {
        public pishinemodel()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("سازمان")
        , MaxLength(20, ErrorMessage = "حداکثر طول نام سازمان 20 کاراکتر است")]
        public string sazmanname { get; set; }
        [DisplayName("عنوان")
        , MaxLength(20, ErrorMessage = "حداکثر طول عنوان 20 کاراکتر است")]
        public string title { get; set; }
        [DisplayName("سطح")
        , MaxLength(20, ErrorMessage = "حداکثر طول سطح 20 کاراکتر است")]
        public string sath { get; set; }
        [DisplayName("سال شروع")
        , MaxLength(4, ErrorMessage = "حداکثر طول سال شروع 4 کاراکتر است")]
        public string shoro { get; set; }
        [DisplayName("سال پایان")
        , MaxLength(4, ErrorMessage = "حداکثر طول سال پایان 4 کاراکتر است")]
        public string payan { get; set; }
        [DisplayName("توضیح")]
        public string tozih { get; set; }
        [Required(ErrorMessage = "نام گروه را وارد نمائید")]
        public int shoghlgropid { get; set; }
        public int rez { get; set; }

        [MaxLength(128)]
        public string usersid { get; set; }
        }
}