﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class projectmodel
    {
        public projectmodel()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("عنوان")
        , MaxLength(20, ErrorMessage = "حداکثر طول عنوان 20 کاراکتر است")]
        public string title { get; set; }
        [DisplayName("سمت")
        , MaxLength(20, ErrorMessage = "حداکثر طول سمت 20 کاراکتر است")]
        public string semat { get; set; }
        [DisplayName("سال")
        , MaxLength(4, ErrorMessage = "حداکثر طول سال 4 کاراکتر است")]
        public string sal { get; set; }
        [DisplayName("توضیح")]
        public string tozih { get; set; }
        public int rez { get; set; }

        [MaxLength(128)]
        public string usersid { get; set; }
    }
}