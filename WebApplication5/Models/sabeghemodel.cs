﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class sabeghemodel
    {
        public sabeghemodel()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("سال")
        , MaxLength(4, ErrorMessage = "حداکثر طول سال 4 کاراکتر است")]
        public string sal { get; set; }
        [DisplayName("توضیح")
        , MaxLength(50, ErrorMessage = "حداکثر طول توضیح 50 کاراکتر است")]
        public string tozih { get; set; }
        public int rezkary { get; set; }
        public int reztahsil { get; set; }

        [MaxLength(128)]
        public string usersid { get; set; }
    }
}