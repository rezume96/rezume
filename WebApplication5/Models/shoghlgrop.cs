﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class shoghlgrop
    {
        public shoghlgrop()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("شغل")
        , MaxLength(50, ErrorMessage = "حداکثر طول نام 50 کاراکتر است")]
        public string name { get; set; }
        public virtual IList<pishine> pishines { get; set; }
    }
}