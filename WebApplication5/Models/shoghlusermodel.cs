﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class shoghlusermodel
    {
        public shoghlusermodel()
        {

        }
        [Key]
        public int id { get; set; }
        public int shoghlid { get; set; }
        public int rez { get; set; }

        [MaxLength(128)]
        public string usersid { get; set; }
    }
}