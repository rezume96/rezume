﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class shoghluser
    {
        #region Configuration
        internal class configuretion : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<shoghluser>
        {
            public configuretion()
            {
                HasRequired(s => s.shoghls).WithMany(s => s.shoghlusers).HasForeignKey(s => s.shoghlid).WillCascadeOnDelete(false);
            }
        }
        #endregion Configuration
        public shoghluser()
        {

        }
        [Key]
        public int id { get; set; }
        public virtual int shoghlid { get; set; }
        public virtual shoghl shoghls { get; set; }
        public int rez { get; set; }

        [MaxLength(128)]
        public virtual string usersid { get; set; }
        public virtual ApplicationUser users { get; set; }
    }
}