﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class tahsilmodel
    {
        public tahsilmodel()
        {

        }
        [Key]
        public int id { get; set; }

        [DisplayName("رشته")
        , MaxLength(50, ErrorMessage = "حداکثر طول رشته 50 کاراکتر است")]
        public string reshteh { get; set; }
        [DisplayName("گرایش")
        , MaxLength(50, ErrorMessage = "حداکثر طول گرایش 50 کاراکتر است")]
        public string gerayesh { get; set; }
        [DisplayName("دانشگاه")
        , MaxLength(50, ErrorMessage = "حداکثر طول دانشگاه 50 کاراکتر است")]
        public string uni { get; set; }
        [DisplayName("مقطع")
        , MaxLength(50, ErrorMessage = "حداکثر طول مطقع 50 کاراکتر است")]
        public string maghta { get; set; }
        [DisplayName("معدل")
        , MaxLength(5, ErrorMessage = "حداکثر طول معدل 5 کاراکتر است")]
        public string moadel { get; set; }
        [DisplayName("سال شروع")
        , MaxLength(4, ErrorMessage = "حداکثر طول سال شروع 4 کاراکتر است")]
        public string shoro { get; set; }
        [DisplayName("سال پایان")
        , MaxLength(4, ErrorMessage = "حداکثر طول سال پایان 4 کاراکتر است")]
        public string payan { get; set; }
        [DisplayName("توضیح")]
        public string tozih { get; set; }
        public int rezkary { get; set; }
        public int reztahsil { get; set; }

        [MaxLength(128)]
        public virtual string usersid { get; set; }
    }
}