﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class usermodel
    {
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string pname { get; set; }
        [MaxLength(10)]
        public string shenasname { get; set; }

        [DisplayName("تاریخ ثبت")]
        [Column(TypeName = "datetime2")]
        public System.DateTime? D_tavalod { get; set; }
        [MaxLength(10)]
        public string mojarad { get; set; }
        [MaxLength(10)]
        public string jens { get; set; }
        [MaxLength(20)]
        public string khedmat { get; set; }
        [MaxLength(11)]
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string shahr { get; set; }
        public string eshteghal { get; set; }
        public string pictur { get; set; }
        public string tozih { get; set; }
        public string alaghe { get; set; }
        public string mahalkar { get; set; }
        public string hoghogh { get; set; }
        public string profil { get; set; }
        public int tedadrezkary { get; set; }
        public int tedadreztahsil { get; set; }
        public string email { get; set; }
        public string phon { get; set; }
        public string id { get; set; }
    }
}