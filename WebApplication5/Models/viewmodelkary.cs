﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class viewmodelkary
    {
        public List<danesh> dan { get; set; }
        public List<eftekhar> eft { get; set; }
        public List<narm> narm { get; set; }
        public List<pdf> pdf { get; set; }
        public List<pishine> pish { get; set; }
        public List<project> proj { get; set; }
        public List<sabeghe> sab { get; set; }
        public List<shoghluser> shogh { get; set; }
        public List<tahsil >tah { get; set; }
        public List<ApplicationUser >user { get; set; }
        public List<zabanuser> zaban { get; set; }
    }
}