﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class zaban
    {
        public zaban()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("نام")
        , MaxLength(20, ErrorMessage = "حداکثر طول نام 20 کاراکتر است")]
        public string name { get; set; }
        public virtual IList<zabanuser> zabanusers { get; set; }
    }
}