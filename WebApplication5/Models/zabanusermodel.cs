﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication5.Models
{
    public class zabanusermodel
    {
        public zabanusermodel()
        {

        }
        [Key]
        public int id { get; set; }
        [DisplayName("میزان تسلط")
        , MaxLength(20, ErrorMessage = "حداکثر طول میزان تسلط 20 کاراکتر است")]
        public string mizan { get; set; }
        [DisplayName("عنوان مدرک")
        , MaxLength(20, ErrorMessage = "حداکثر طول عنوان مدرک 20 کاراکتر است")]
        public string madrak { get; set; }
        public int zabanid { get; set; }
        public int rezkary { get; set; }
        public int reztahsil { get; set; }

        [MaxLength(128)]
        public string usersid { get; set; }
    }
}